package com.databright.llrp.client.unittest;

import org.apache.log4j.BasicConfigurator;

import com.redskyconsulting.unittest.AbstractUnitTest;
import com.redskyconsulting.utility.ToolBox;



/**
 * <p>Title: ClientTest</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public class ClientTest
  extends AbstractUnitTest
{
  
  public ClientTest()
  {
    super();
  }

  @Override
  protected void Prologue()
  {
    ToolBox.Log("Entering");
    try
    {
      // todo
    }
    finally
    {
      ToolBox.Log("Exiting");
    }
  }
  
  @Override
  protected void DoTest() 
  {
    ToolBox.Log("Entering");
    try
    {
      // todo
    }
    finally
    {
      ToolBox.Log("Exiting");
    }
  }

  @Override
  protected void Epilogue()
  {
    ToolBox.Log("Entering");
    try
    {
      // todo
    }
    finally
    {
      ToolBox.Log("Exiting");
    }
  }

  
  public static void main(String[] args)
  {
    BasicConfigurator.configure();
    ClientTest clientTest = new ClientTest();
    clientTest.Test();
  }

}
