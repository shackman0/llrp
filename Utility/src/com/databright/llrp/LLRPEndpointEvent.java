package com.databright.llrp;

import org.llrp.ltk.types.LLRPMessage;

import com.redskyconsulting.utility.EventNotificationManagerEvent;

/**
 * <p>Title: LLRPMessageReceivedEvent</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public class LLRPEndpointEvent<Broadcaster>
  extends EventNotificationManagerEvent<Broadcaster>
{

  
  public LLRPEndpointEvent(
    Broadcaster  broadcaster_, 
    boolean      success_ )
  {
    super(broadcaster_, success_);
  }

  public LLRPEndpointEvent(
    Broadcaster   broadcaster_,
    boolean       success_,
    Object        other_ )
  {
    super(broadcaster_, success_, other_);
  }

  public LLRPMessage GetLLRPMessage()
  {
    return (LLRPMessage)GetOther();
  }

  
}
