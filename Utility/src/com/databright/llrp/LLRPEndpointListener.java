package com.databright.llrp;

/**
 * <p>Title: LLRPMessageReceivedListener</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public interface LLRPEndpointListener<Broadcaster>
{

  public enum CallbackMethods
  {
    LLRPErrorOccurred,
    LLRPMessageReceived,
  }

  /**
   * @param llrpMessageReceivedEvent
   */
  public void LLRPErrorOccurred(LLRPEndpointEvent<Broadcaster> llrpMessageReceivedEvent);
  
  /**
   * the message has been received and added to the message queue
   * @param llrpMessageReceivedEvent
   */
  public void LLRPMessageReceived(LLRPEndpointEvent<Broadcaster> llrpMessageReceivedEvent);
  
}
