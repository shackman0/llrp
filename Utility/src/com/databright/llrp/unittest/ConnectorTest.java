package com.databright.llrp.unittest;

import org.apache.log4j.BasicConfigurator;
import org.llrp.ltk.net.LLRPConnectionAttemptFailedException;

import com.databright.llrp.AbstractEndpoint;
import com.databright.llrp.Connector;
import com.redskyconsulting.unittest.AbstractUnitTest;
import com.redskyconsulting.utility.ToolBox;

/**
 * <p>Title: ConnectorTest</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public class ConnectorTest
  extends AbstractUnitTest
{

  protected String        _host;
  protected int           _port;
  protected Connector     _client; 

  
  public ConnectorTest(
    String    host_,
    int       port_ )
  {
    super();

    _host = host_;
    _port = port_;
  }
  

  @Override
  protected void Prologue()
  {
    ToolBox.Log("Entering");
    try
    {
      _client = new Connector();
    }
    finally
    {
      ToolBox.Log("Exiting");
    }
  }
  
  @Override
  protected void DoTest() 
    throws LLRPConnectionAttemptFailedException
  {
    ToolBox.Log("Entering");
    try
    {
      _client.Connect(new Endpoint(), _host,_port);
    }
    finally
    {
      ToolBox.Log("Exiting");
    }
  }

  @Override
  protected void Epilogue()
  {
    ToolBox.Log("Entering");
    try
    {
      _client.Disconnect();
    }
    finally
    {
      ToolBox.Log("Exiting");
    }
  }


  public static void main(String[] args)
  {
    BasicConfigurator.configure();
    
    ConnectorTest llrpClientTest = new ConnectorTest("tbd",5084);
    llrpClientTest.Test();
  }
  
  class Endpoint
    extends AbstractEndpoint
  {
    
    public Endpoint()
    {
      super();
    }
    
  }

}
