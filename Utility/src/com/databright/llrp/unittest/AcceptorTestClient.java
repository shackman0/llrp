package com.databright.llrp.unittest;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.BasicConfigurator;
import org.llrp.ltk.exceptions.InvalidLLRPMessageException;
import org.llrp.ltk.generated.messages.KEEPALIVE;
import org.llrp.ltk.net.LLRPConnectionAttemptFailedException;

import com.databright.llrp.AbstractEndpoint;
import com.redskyconsulting.unittest.AbstractUnitTest;
import com.redskyconsulting.utility.ToolBox;

/**
 * <p>
 * Title: AcceptorTestClient
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2010
 * </p>
 * <p>
 * Company: Databright Management Systems.
 * </p>
 * 
 * @author Floyd Shackelford
 */
public class AcceptorTestClient
  extends AbstractUnitTest
{

   protected static final String HOST = "64.130.106.101";
//  protected static final String HOST = "localhost";

  protected String              _host;

  protected int                 _port;

  public AcceptorTestClient(String host_, int port_)
  {
    super();

    _host = host_;
    _port = port_;
  }

  @Override
  protected void Prologue()
  {
    ToolBox.Log("Entering");
    try
    {
      // do nothing
    }
    finally
    {
      ToolBox.Log("Exiting");
    }
  }

  @Override
  protected void DoTest() 
    throws LLRPConnectionAttemptFailedException, TimeoutException
  {
    ToolBox.Log("Entering");
    try
    {
        InetAddress inetAddress = InetAddress.getByName(_host); 
        SocketAddress socketAddress = new InetSocketAddress(inetAddress, _port); 
        Socket socket = new Socket(); 
        int timeoutMillis = 5000; 
        socket.connect(socketAddress, timeoutMillis); 
        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
        
        KEEPALIVE keepalive = new KEEPALIVE();
        dataOutputStream.write(keepalive.encodeBinary());
        ToolBox.Pause(250);
        
        socket.close();
    }        
    catch (UnknownHostException ex) 
    { 
      ex.printStackTrace();
    } 
    catch (SocketTimeoutException ex) 
    { 
      ex.printStackTrace();
    } 
    catch (IOException ex) 
    { 
      ex.printStackTrace();
    }
    catch (InvalidLLRPMessageException ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      ToolBox.Log("Exiting");
    }
  }

  @Override
  protected void Epilogue()
  {
    ToolBox.Log("Entering");
    try
    {
      // do nothing
    }
    finally
    {
      ToolBox.Log("Exiting");
    }
  }

  public static void main(String[] args)
  {
    BasicConfigurator.configure();
    AcceptorTestClient serverTestClient = new AcceptorTestClient(HOST, AcceptorTest.DEFAULT_PORT);
    serverTestClient.Test();
  }

  class Endpoint
    extends AbstractEndpoint
  {

    public Endpoint()
    {
      super();
    }

  }

}
