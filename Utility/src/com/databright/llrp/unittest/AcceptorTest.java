package com.databright.llrp.unittest;

import org.apache.log4j.BasicConfigurator;
import org.llrp.ltk.net.LLRPConnectionAttemptFailedException;
import org.llrp.ltk.types.LLRPMessage;

import com.databright.llrp.AbstractEndpoint;
import com.databright.llrp.Acceptor;
import com.redskyconsulting.unittest.AbstractUnitTest;
import com.redskyconsulting.utility.ToolBox;


/**
 * <p>
 * Title: AcceptorTest
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2010
 * </p>
 * <p>
 * Company: Databright Management Systems.
 * </p>
 * 
 * @author Floyd Shackelford
 */
public class AcceptorTest
  extends AbstractUnitTest
{

  public static final int DEFAULT_PORT = 5084;

  protected int        _port = DEFAULT_PORT;
  protected Acceptor   _acceptor; 

  
  
  public AcceptorTest()
  {
    super();
  }
  
  public AcceptorTest(int port_)
  {
    super();
    
    _port = port_;
  }


  @Override
  protected void Prologue()
  {
    ToolBox.Log("Entering");
    try
    {
      _acceptor = new Acceptor(null);
    }
    finally
    {
      ToolBox.Log("Exiting");
    }
  }
  
  @Override
  protected void DoTest() 
    throws LLRPConnectionAttemptFailedException
  {
    ToolBox.Log("Entering");
    try
    {
      _acceptor.Bind(_port, new Endpoint());
    }
    finally
    {
      ToolBox.Log("Exiting");
    }
  }

  @Override
  protected void Epilogue()
  {
    ToolBox.Log("Entering");
    try
    {
//    _llrpServer.Close();
    }
    finally
    {
      ToolBox.Log("Exiting");
    }
  }


  public static void main(String[] args)
  {
    BasicConfigurator.configure();
    AcceptorTest acceptorTest = new AcceptorTest();
    acceptorTest.Test();
  }

  class Endpoint
    extends AbstractEndpoint
  {
    
    public Endpoint()
    {
      super();
    }

    @Override
    public void messageReceived(LLRPMessage llrpMessage_)
    {
      super.messageReceived(llrpMessage_);
      
//      KEEPALIVE_ACK keepAliveAck = new KEEPALIVE_ACK();
//      _acceptor.Send(keepAliveAck);
    }
    
  }
  
}

