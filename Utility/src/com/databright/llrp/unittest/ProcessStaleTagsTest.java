package com.databright.llrp.unittest;

import org.apache.log4j.BasicConfigurator;

import com.databright.llrp.ProcessStaleTags;
import com.databright.llrp.TagReportInformation;
import com.databright.llrp.server.ServerRuntimeProperties;
import com.redskyconsulting.unittest.AbstractUnitTest;
import com.redskyconsulting.utility.ToolBox;





public class ProcessStaleTagsTest
  extends AbstractUnitTest
{
  
  protected ProcessStaleTags _processStaleTags;
  
  
  public ProcessStaleTagsTest()
  {
    super();
  }


  @Override
  protected void Prologue()
    throws Exception
  {
    ToolBox.Log("Entering");
    try
    {
      _processStaleTags = new ProcessStaleTags();
      _processStaleTags.SetSync2Helix(false);
      Thread thread = new Thread(_processStaleTags);
      thread.start();
    }
    finally
    {
      ToolBox.Log("Exiting");
    }
  }

  protected static final long PROCESS_STALE_TAGS_SLEEP_MILLIS = 100L;
  protected static final long STALE_TAG_THRESHOLD_MILLIS = 500L;
  protected static final long PAUSE_FOR_STALE_TAG_THRESHOLD_MILLIS = 1000L;
  
  
  @Override
  protected void DoTest()
    throws Exception
  {
    ToolBox.Log("Entering");
    try
    {
      ServerRuntimeProperties.Property.ProcessStaleTagsSleepMillis.Set(PROCESS_STALE_TAGS_SLEEP_MILLIS);
      ServerRuntimeProperties.Property.StaleTagThresholdMillis.Set(STALE_TAG_THRESHOLD_MILLIS);

      // should process these tags as stale
      new TagReportInformation("10","20","30","n/a");
      ToolBox.Pause(PROCESS_STALE_TAGS_SLEEP_MILLIS);
      new TagReportInformation("11","21","31","n/a");
      ToolBox.Pause(PROCESS_STALE_TAGS_SLEEP_MILLIS);
      new TagReportInformation("12","22","32","n/a");
      ToolBox.Pause(PROCESS_STALE_TAGS_SLEEP_MILLIS);

      ToolBox.Pause(PAUSE_FOR_STALE_TAG_THRESHOLD_MILLIS);

      // should complain about leaving a tag behind
      new TagReportInformation("13","23","33","n/a");
      _processStaleTags.Exit();
      ToolBox.Pause(PAUSE_FOR_STALE_TAG_THRESHOLD_MILLIS);
    }
    finally
    {
      ToolBox.Log("Exiting");
    }
  }



  @Override
  protected void Epilogue()
    throws Exception
  {
    ToolBox.Log("Entering");
    try
    {
      // do nothing
    }
    finally
    {
      ToolBox.Log("Exiting");
    }
  }

  
  public static void main(String... args)
  {
    BasicConfigurator.configure();
    ServerRuntimeProperties.GetInstance();
    
    ProcessStaleTagsTest processStaleTagsTest = new ProcessStaleTagsTest();
    processStaleTagsTest.Test();
    
    System.exit(0);
  }
}
