package com.databright.llrp;

import org.llrp.ltk.net.LLRPEndpoint;
import org.llrp.ltk.types.LLRPMessage;

import com.redskyconsulting.utility.ToolBox;



/**
 * <p>Title: AbstractEndpoint</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public abstract class AbstractEndpoint
  implements LLRPEndpoint
{

  /**
   * when false, both error messages and llrp messages will be ignored
   */
  protected boolean        _processMessages = true;

  
  public AbstractEndpoint()
  {
    super();
  }

  public boolean IsProcessingMessages()
  {
    return _processMessages;
  }
  
  public boolean GetProcessMessages()
  {
    return _processMessages;
  }

  public void SetProcessMessages(boolean processMessages_)
  {
    _processMessages = processMessages_;
  }

  @Override
  public void errorOccured(String errorMessage_)
  {
    String errorMessage = errorMessage_;
    if (_processMessages == false)
    {
      errorMessage = "IGNORED: " + errorMessage;
    }
    ToolBox.Log(errorMessage);
  }
  
  @Override
  public void messageReceived(LLRPMessage llrpMessage_)
  {
    String messageClass = llrpMessage_.getClass().getSimpleName();
    if (_processMessages == false)
    {
      messageClass = "IGNORED: " + messageClass;
    }
//    ToolBox.Log(messageClass);
//    try
//    {
//      ToolBox.Log(llrpMessage_.toXMLString());
//    }
//    catch (InvalidLLRPMessageException ex)
//    {
//      ex.printStackTrace();
//    }
  }

}
