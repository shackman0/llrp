package com.databright.llrp;

import java.util.concurrent.LinkedBlockingQueue;

import org.llrp.ltk.types.LLRPMessage;

import com.databright.llrp.server.ServerRuntimeProperties;



/**
 * <p>Title: AbstractQueuedEndpoint</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public abstract class AbstractQueuedEndpoint
  extends AbstractEndpoint
{
  

  protected LinkedBlockingQueue<LLRPMessage> _llrpMessageQueue;

  protected LinkedBlockingQueue<String> _errorMessageQueue;

  
  
  public AbstractQueuedEndpoint()
  {
    super();
    
    _llrpMessageQueue = new LinkedBlockingQueue(ServerRuntimeProperties.Property.LLRPMessagesQueueCapacity.GetAsInteger());
    _errorMessageQueue = new LinkedBlockingQueue(ServerRuntimeProperties.Property.ErrorMessagesQueueCapacity.GetAsInteger());
  }
  
  public LinkedBlockingQueue<LLRPMessage> GetLLRPMessageQueue()
  {
    return _llrpMessageQueue;
  }

  @Override
  public void errorOccured(String errorMessage_)
  {
    super.errorOccured(errorMessage_);

    if (_processMessages == true)
    {
      _errorMessageQueue.add(errorMessage_);
    }
  }
  
  @Override
  public void messageReceived(LLRPMessage llrpMessage_)
  {
    super.messageReceived(llrpMessage_);

    if (_processMessages == true)
    {
      _llrpMessageQueue.add(llrpMessage_);
    }
  }

}
