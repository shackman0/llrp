package com.databright.llrp;

import java.net.InetAddress;
import java.net.InetSocketAddress;

import org.apache.mina.common.IoSession;
import org.llrp.ltk.generated.messages.CLOSE_CONNECTION_RESPONSE;
import org.llrp.ltk.generated.messages.GET_READER_CONFIG_RESPONSE;
import org.llrp.ltk.generated.messages.READER_EVENT_NOTIFICATION;
import org.llrp.ltk.generated.messages.RO_ACCESS_REPORT;
import org.llrp.ltk.generated.parameters.Identification;
import org.llrp.ltk.net.LLRPIoHandlerAdapterImpl;
import org.llrp.ltk.types.LLRPMessage;

import com.redskyconsulting.utility.HashMap;
import com.redskyconsulting.utility.ToolBox;

/**
 * <p>
 * Title: LLRPIoHandler
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2010
 * </p>
 * <p>
 * Company: Databright Management Systems.
 * </p>
 * 
 * @author Floyd Shackelford
 */
public class LLRPIOHandler
  extends LLRPIoHandlerAdapterImpl
{

  /**
   * key = InetAddress
   * value = Device Identifier for the device at InetAddress
   * 
   * todo: should store the entire GET_READER_CONFIG_RESPONSE instead of just the device id
   */
  private static HashMap<InetAddress, String>          __inetAddress2DeviceIdentifierMap;
  static
  {
    __inetAddress2DeviceIdentifierMap = new HashMap();
  }

  public static String GetDeviceIdentifer(InetAddress inetAddress_)
  {
    String deviceIdentifier = __inetAddress2DeviceIdentifierMap.get(inetAddress_);
    if (deviceIdentifier == null)
    {
      ToolBox.Log("Cannot find DeviceIdentifier in map for InetAddress " + inetAddress_);
    }

    return deviceIdentifier;
  }

  public static void PutDeviceIdentifier(
    InetAddress inetAddress_,
    String      deviceIdentifier_ )
  {
    LLRPIOHandler.__inetAddress2DeviceIdentifierMap.put(inetAddress_, deviceIdentifier_);
    ToolBox.Log("Map.put() inetAddress " + inetAddress_ + ", deviceIdentifier " + deviceIdentifier_);
  }
  
  public static void PutDeviceIdentifier(GET_READER_CONFIG_RESPONSE getReaderConfigResponse_)
  {
    Identification identification = getReaderConfigResponse_.getIdentification();
    String identificationString = identification.toString().trim();
    String deviceIdentifier = identificationString.substring(identificationString.lastIndexOf(" ") + 1);
    
    InetAddress inetAddress = LLRPIOHandler.GetInetAddress(getReaderConfigResponse_);
    if (inetAddress != null)
    {
      PutDeviceIdentifier(inetAddress, deviceIdentifier);
    }
  }

  public static String RemoveDeviceIdentifier(InetAddress inetAddress_)
  {
    String deviceIdentifier = __inetAddress2DeviceIdentifierMap.remove(inetAddress_);
    if (deviceIdentifier == null)
    {
      ToolBox.Log("Cannot find DeviceIdentifier in map for InetAddress " + inetAddress_);
    }
    
    return deviceIdentifier;
  }

  public static String RemoveDeviceIdentifier(CLOSE_CONNECTION_RESPONSE closeConnectionResponse_)
  {
    InetAddress inetAddress = GetInetAddress(closeConnectionResponse_);
    String deviceIdentifier = __inetAddress2DeviceIdentifierMap.remove(inetAddress);
    if (deviceIdentifier == null)
    {
      ToolBox.Log("Cannot find DeviceIdentifier in map for InetAddress " + inetAddress);
    }
    
    return deviceIdentifier;
  }
  
  
  /**
   * key = LLRPMessage 
   * value = InetAddress associated with the LLRPMessage
   */
  private static HashMap<LLRPMessage,InetAddress>      __llrpMessage2InetAddressMap;
  static
  {
    __llrpMessage2InetAddressMap = new HashMap();
  }

  public static InetAddress GetInetAddress(LLRPMessage llrpMessage_)
  {
    InetAddress inetAddress = __llrpMessage2InetAddressMap.get(llrpMessage_);
    if (inetAddress == null)
    {
      ToolBox.Log("Cannot find InetAddress in map for LLRPMessage " + llrpMessage_.getName());
    }

    return inetAddress;
  }

  public static void PutInetAddress(
    LLRPMessage llrpMessage_, 
    InetAddress inetAddress_)
  {
    __llrpMessage2InetAddressMap.put(llrpMessage_,inetAddress_);
    ToolBox.Log("Map.put() llrpMessage " + llrpMessage_.getName() + ", inetAddress " + inetAddress_);
  }

  public static InetAddress RemoveInetAddress(LLRPMessage llrpMessage_)
  {
    InetAddress inetAddress = __llrpMessage2InetAddressMap.remove(llrpMessage_);
    if (inetAddress == null)
    {
      ToolBox.Log("Cannot find InetAddress in map for LLRPMessage " + llrpMessage_.getName());
    }
    
    return inetAddress;
  }
  
  
  
  public static String GetDeviceIdentifier(LLRPMessage llrpMessage)
  {
    String deviceIdentifier = null;
    
    InetAddress inetAddress = GetInetAddress(llrpMessage);
    if (inetAddress != null)
    {
      deviceIdentifier = GetDeviceIdentifer(inetAddress);
    }
    
    return deviceIdentifier;
  }
  


  
  protected Acceptor _acceptor;
  
  
  
  public LLRPIOHandler(Acceptor acceptor_)
  {
    super(acceptor_.GetLLRPAcceptor());
    
    _acceptor = acceptor_;
  }

  @Override
  public void messageReceived(IoSession session, Object message)
    throws Exception 
  {
    InetSocketAddress inetSocketAddress = (InetSocketAddress)session.getRemoteAddress();
    InetAddress inetAddress = inetSocketAddress.getAddress();

    LLRPMessage llrpMessage = (LLRPMessage) message;
    if ( (llrpMessage instanceof RO_ACCESS_REPORT) ||
         (llrpMessage instanceof READER_EVENT_NOTIFICATION) ||
         (llrpMessage instanceof GET_READER_CONFIG_RESPONSE) ||
         (llrpMessage instanceof CLOSE_CONNECTION_RESPONSE) )
    {
      PutInetAddress(llrpMessage,inetAddress);
    }
    
    super.messageReceived(session, message);

    /*
     * ReaderEventNotificationHandler.ProcessConnectionAttemptEvent() runs is a separate thread and
     * will do it's own RemoveInetAddress() 
     */
    if ( (llrpMessage instanceof RO_ACCESS_REPORT) ||
         (llrpMessage instanceof GET_READER_CONFIG_RESPONSE) ||
         (llrpMessage instanceof CLOSE_CONNECTION_RESPONSE) )
    {
      RemoveInetAddress(llrpMessage);
    }
    
    /*
     * for debugging purposes
     * to ensure that the maps are properly cleaned up by threaded handlers like READER_EVENT_NOTIFICATION 
     */
    if (llrpMessage instanceof CLOSE_CONNECTION_RESPONSE)
    {
      if (__llrpMessage2InetAddressMap.size() == 0)
      {
        ToolBox.Log("__llrpMessage2InetAddressMap is empty.");
      }
      else
      {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("contents of __inetAddress2DeviceIdentifierMap\n");
        for (LLRPMessage llrpMessageKey: __llrpMessage2InetAddressMap.keySet())
        {
          stringBuilder.append("  ");
          stringBuilder.append(llrpMessageKey);
          stringBuilder.append("\n");
        }
        ToolBox.Log(stringBuilder.toString());
      }
      
      if (__inetAddress2DeviceIdentifierMap.size() == 0)
      {
        ToolBox.Log("__inetAddress2DeviceIdentifierMap is empty.");
      }
      else
      {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("contents of __inetAddress2DeviceIdentifierMap\n");
        for (InetAddress inetAddressKey: __inetAddress2DeviceIdentifierMap.keySet())
        {
          stringBuilder.append("  ");
          stringBuilder.append(inetAddressKey);
          stringBuilder.append("\n");
        }
        ToolBox.Log(stringBuilder.toString());
      }
    }
  }

  

}
