package com.databright.llrp;

import java.util.concurrent.TimeoutException;

import org.llrp.ltk.net.LLRPConnectionAttemptFailedException;
import org.llrp.ltk.net.LLRPConnector;
import org.llrp.ltk.types.LLRPMessage;

/**
 * <p>Title: Client</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public class Connector
{

  protected LLRPConnector       _llrpConnector = null;

  
  public Connector()
  {
    super();
  }
  
  public LLRPConnector GetLLRPConnector()
  {
    return _llrpConnector;
  }

  public void Connect( 
    AbstractEndpoint endpoint_,
    String           host_,
    int              port_ )
    throws LLRPConnectionAttemptFailedException 
  {
    _llrpConnector = new LLRPConnector(endpoint_, host_, port_);
    _llrpConnector.connect();
  }
  
  public void Send(LLRPMessage llrpMessage)
  {
    _llrpConnector.send(llrpMessage);
  }
  
  public LLRPMessage Transact(LLRPMessage llrpMessage) 
    throws TimeoutException
  {
    return _llrpConnector.transact(llrpMessage);
  }
  
  public void Disconnect()
  {
    _llrpConnector.disconnect();
    _llrpConnector = null;
  }
  
  

}
