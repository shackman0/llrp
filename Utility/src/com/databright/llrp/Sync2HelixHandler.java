package com.databright.llrp;

import java.io.IOException;
import java.text.MessageFormat;

import com.databright.llrp.server.ServerRuntimeProperties;
import com.redskyconsulting.utility.ToolBox;




/**
 * <p>Title: Sync2Helix</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public class Sync2HelixHandler
{
  
  
  /**
   * 0 = ServerRuntimeProperties.Property.Sync2HelixScript
   * 1 = event type
   * 2 = device identifier
   * 3 = antenna identifier
   * 4 = tag identifier
   * 5 = read dts 
   */
  public static final String SYNC_2_HELIX_FORMAT = 
    "{0} -event-type \"{1}\" -device-identifier \"{2}\" -antenna-identifier \"{3}\" -tag-identifier \"{4}\" -read-date-time \"{5}\"";

  
  public enum EventType
  {
    NewTag
    {
      @Override
      public String GetEventCode()
      {
        return "101";
      }
    },
    StaleTag
    {
      @Override
      public String GetEventCode()
      {
        return "201";
      }
    };
    public abstract String GetEventCode();
  }

  
  protected TagReportInformation _tagReportInformation;
  protected EventType            _eventType; 
  
  
  public Sync2HelixHandler(
    TagReportInformation tagReportInformation_,
    EventType            eventType_ )

  {
    super();

    _tagReportInformation = tagReportInformation_;
    _eventType = eventType_;
  }

  public void Sync2Helix()
  {
    try
    {
      Runtime.getRuntime().exec(GetSync2HelixString());
    }
    catch (IOException ex)
    {
      ToolBox.Log(ex);
    }
  }
  
  public String GetSync2HelixString()
  {
    String sync2Helix = MessageFormat.format(
      SYNC_2_HELIX_FORMAT,
      ServerRuntimeProperties.Property.Sync2HelixScript.GetAsString(),
      _eventType.GetEventCode(),
      _tagReportInformation.GetDeviceIdentifier(),
      _tagReportInformation.GetAntennaIdentifier(),
      _tagReportInformation.GetTagIdentifier(),
      _tagReportInformation.GetFirstSeenDTS());
    
    ToolBox.Log(sync2Helix);
    
    return sync2Helix;
  }

  
}
