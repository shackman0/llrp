package com.databright.llrp.server;

import com.redskyconsulting.utility.AbstractRuntimeProperties;





/**
 * <p>Title: ServerRuntimeProperties</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public class ServerRuntimeProperties
  extends AbstractRuntimeProperties
{

  private static final long serialVersionUID = 1L;

  protected static final String   DEFAULT_PAUSE_AFTER_SEND_MILLIS = "3000"; 
  protected static final String   DEFAULT_PORT = "5084";
  protected static final String   DEFAULT_LLRP_MESSAGES_QUEUE_CAPACITY = "100";
  protected static final String   DEFAULT_ERROR_MESSAGES_QUEUE_CAPACITY = "100";
  protected static final String   DEFAULT_PROCESS_STALE_TAGS_SLEEP_MILLIS = "3000";
  protected static final String   DEFAULT_STALE_TAG_THRESHOLD_MILLIS = "10000";
  protected static final String   DEFAULT_SYNC_2_HELIX_SCRIPT = "./runtime/LLRP_Sync_To_Helix.php";
  


  public enum Property
  {
    Sync2HelixScript
    {
      @Override
      public String GetDefaultValue()
      {
        return DEFAULT_SYNC_2_HELIX_SCRIPT;
      }
    },

    Port
    {
      @Override
      public String GetDefaultValue()
      {
        return DEFAULT_PORT;
      }
    },

    PauseAfterSendMillis
    {
      @Override
      public String GetDefaultValue()
      {
        return DEFAULT_PAUSE_AFTER_SEND_MILLIS;
      }
    },

    LLRPMessagesQueueCapacity
    {
      @Override
      public String GetDefaultValue()
      {
        return DEFAULT_LLRP_MESSAGES_QUEUE_CAPACITY;
      }
    },
    
    ErrorMessagesQueueCapacity
    {
      @Override
      public String GetDefaultValue()
      {
        return DEFAULT_ERROR_MESSAGES_QUEUE_CAPACITY;
      }
    },

    ProcessStaleTagsSleepMillis
    {
      @Override
      public String GetDefaultValue()
      {
        return DEFAULT_PROCESS_STALE_TAGS_SLEEP_MILLIS;
      }
    },

    StaleTagThresholdMillis
    {
      @Override
      public String GetDefaultValue()
      {
        return DEFAULT_STALE_TAG_THRESHOLD_MILLIS;
      }
    };

    public String GetAsString()
    {
      String value = GetInstance().get(name());
      
      if (value == null)
      {
        return Set2Default();
      }
      
      return value.trim();
    }

    public Long GetAsLong()
    {
      return Long.valueOf(GetAsString());
    }

    public Integer GetAsInteger()
    {
      return Integer.valueOf(GetAsString());
    }

    /**
     * @param newValue
     * @return previousValue or null
     */
    public String Set(String newValue_)
    {
      return GetInstance().put(name(),String.valueOf(newValue_));
    }

    /**
     * @param newValue
     * @return previousValue or null
     */
    public String Set(int newValue_)
    {
      return Set(String.valueOf(newValue_));
    }

    /**
     * @param newValue
     * @return previousValue or null
     */
    public String Set(long newValue_)
    {
      return Set(String.valueOf(newValue_));
    }

    /**
     * @return the value before being unset
     */
    public String Unset()
    {
      return GetInstance().remove(name());
    }

    /**
     * @return the default value
     */
    public String Set2Default()
    {
      Set(GetDefaultValue());
      
      return GetAsString();
    }
    
    public abstract String GetDefaultValue();
  }
  

  static 
  {
    __runtimeProperties = new ServerRuntimeProperties("./runtime/server.properties");
    __runtimeProperties.Load();
    
    // initialize the property list
    for (Property property: Property.values())
    {
      property.GetAsString();
    }
  }

  public static ServerRuntimeProperties GetInstance()
  {
    return (ServerRuntimeProperties)__runtimeProperties;
  }

  

  protected ServerRuntimeProperties()
  {
    super();
  }

  public ServerRuntimeProperties(String  filePathName)
  {
    super(filePathName);
  }

  public ServerRuntimeProperties(
    String  filePathName,
    String  zipFilePathName )
  {
    super(filePathName,zipFilePathName);
  }

  public ServerRuntimeProperties(
    String  filePathName,
    String  zipFilePathName,
    boolean inJarFile )
  {
    super(filePathName,zipFilePathName,inJarFile);
  }

  public ServerRuntimeProperties(
    String  filePathName,
    boolean inJarFile )
  {
    super(filePathName,inJarFile);
  }

  @Override
  protected String GetHeaderString()
  {
    return "Change only the values. Only modifications to values will be preserved.";
  }

}
