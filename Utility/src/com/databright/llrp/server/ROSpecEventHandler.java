package com.databright.llrp.server;

import org.llrp.ltk.generated.enumerations.ROSpecEventType;
import org.llrp.ltk.generated.parameters.ROSpecEvent;

import com.redskyconsulting.utility.ToolBox;





/**
 * <p>Title: ROSpecEventHandler</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public class ROSpecEventHandler
  implements Runnable
{

  protected Server            _server;
  protected ROSpecEvent       _roSpecEvent;
  
  
  
  public ROSpecEventHandler(
    Server      server_, 
    ROSpecEvent roSpecEvent_)
  {
    super();
    
    _server = server_;
    _roSpecEvent = roSpecEvent_;
  }

  @Override
  public void run()
  {
    ROSpecEventType roSpecEventType = _roSpecEvent.getEventType();
    switch (roSpecEventType.toInteger())
    {
      case ROSpecEventType.Start_Of_ROSpec:
        break;
      case ROSpecEventType.Preemption_Of_ROSpec:
        break;
      case ROSpecEventType.End_Of_ROSpec:
        break;
    }
  }
  
  protected void ProcessStartOfROSpec()
  {
    ToolBox.Log("ProcessStartOfROSpec() not implemented.");
  }
  
  protected void ProcessPreemptionOfROSpec()
  {
    ToolBox.Log("ProcessPreemptionOfROSpec() not implemented.");
  }
  
  protected void ProcessEndOfROSpec()
  {
    ToolBox.Log("ProcessEndOfROSpec() not implemented.");
  }
  
}
