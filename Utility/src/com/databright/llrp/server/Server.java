package com.databright.llrp.server;

import java.util.concurrent.TimeoutException;

import org.apache.log4j.BasicConfigurator;
import org.llrp.ltk.generated.messages.CLOSE_CONNECTION;
import org.llrp.ltk.generated.messages.DELETE_ROSPEC;
import org.llrp.ltk.generated.messages.DISABLE_ROSPEC;
import org.llrp.ltk.generated.messages.STOP_ROSPEC;
import org.llrp.ltk.net.LLRPConnectionAttemptFailedException;
import org.llrp.ltk.types.LLRPMessage;
import org.llrp.ltk.types.UnsignedInteger;

import com.databright.llrp.Acceptor;
import com.databright.llrp.ProcessStaleTags;
import com.redskyconsulting.utility.ToolBox;



/**
 * <p>Title: Server</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public class Server
{

  /**
   * The ID of the ROSpec that is used
   */
  public static int ROSPEC_ID = 1;

  
  protected int                 _port;

  protected Acceptor            _acceptor = null;
  protected ServerEndpoint      _serverEndpoint = null;
  protected boolean             _serverStarted = false;
  protected boolean             _connectionEstablished = false;

  
  protected ProcessStaleTags    _processStaleTags;
  
  
  
  public Server()
  {
    super();
    
    _port = ServerRuntimeProperties.Property.Port.GetAsInteger();
  }

  public Server(int port_)
  {
    this();
    
    _port = port_;
  }

  
  public boolean GetConnectionEstablished()
  {
    return _connectionEstablished;
  }
  
  public boolean IsConnectionEstablished()
  {
    return _connectionEstablished;
  }

  public void SetConnectionEstablished(boolean connectionEstablished_)
  {
    _connectionEstablished = connectionEstablished_;
  }

  public boolean GetServerStarted()
  {
    return _serverStarted;
  }

  public boolean IsServerStarted()
  {
    return _serverStarted;
  }

  public int GetPort()
  {
    return _port;
  }
  
  public Acceptor GetAcceptor()
  {
    return _acceptor;
  }
  
  public ServerEndpoint GetEndpoint()
  {
    return _serverEndpoint;
  }

  public void Send(LLRPMessage llrpMessage)
  {
    _acceptor.Send(llrpMessage);
    ToolBox.Pause(ServerRuntimeProperties.Property.PauseAfterSendMillis.GetAsLong());
  }

  public LLRPMessage Transact(LLRPMessage llrpMessage) 
    throws TimeoutException
  {
    return _acceptor.Transact(llrpMessage);
  }
  
  /**
   * Start the server:
   * 1. create a new acceptor and a new endpoint
   * 2. connect to a socket
   * 3. start listening for llrp messages
   * 4. process llrp messages
   * 
   * @throws LLRPConnectionAttemptFailedException 
   */
  public void Start() 
    throws LLRPConnectionAttemptFailedException
  {
    if (_serverStarted == false)
    {
      _acceptor = new Acceptor(this);
      _acceptor.Bind(_port, CreateServerEndpoint());
      
      _processStaleTags = new ProcessStaleTags();
      Thread thread = new Thread(_processStaleTags);
      thread.start();
      
      _serverStarted = true;

      ToolBox.Log("Server started");
    }
  }

  /**
   * Stop the server:
   * 1. stop listening for llrp messages
   * 2. disconnect from the socket
   * 3. the acceptor, along with it's corresponding endpoint and message queue, is discarded.
   */
  public void Stop()
  {
    if (_serverStarted == true)
    {
      if (_connectionEstablished == true)
      {
        DisconnectFromDevice();
      }

      _acceptor.Close();
      _acceptor = null;
      _serverEndpoint = null;

      _processStaleTags.Exit();
      _processStaleTags = null;
      
      _serverStarted = false;
      
      System.gc();

      ToolBox.Log("Server stopped");
    }
  }

  /**
   * pause llrp message processing.
   * llrp message receipt continues and llrp messages continue to be added to the message queue as they arrive.
   * if the Server is not running or running and already paused, no error is generated and the method just returns.
   */
  public void Pause()
  {
    if (_serverEndpoint != null)
    {
      _serverEndpoint.SetProcessMessages(false);
    }
  }
  
  public boolean IsPaused()
  {
    boolean isPaused = true;
    
    if (_serverEndpoint != null)
    {
      isPaused = _serverEndpoint.GetProcessMessages();
    }
    
    return isPaused;
  }

  /**
   * resume llrp message processing.
   * if the Server is not paused, no error is generated and the method just returns.
   */
  public void Resume()
  {
    if (_serverEndpoint != null)
    {
      _serverEndpoint.SetProcessMessages(true);
    }
  }
  
  protected ServerEndpoint CreateServerEndpoint()
  {
    _serverEndpoint = new ServerEndpoint();
    
    ServerEndpointEventHandler serverEndpointEventHandler = new ServerEndpointEventHandler(this);
    _serverEndpoint.GetLLRPEndpointEventNotificationManager().AddListener(serverEndpointEventHandler);
    
    return _serverEndpoint;
  }
  
  
  public void DisconnectFromDevice()
  {
    _connectionEstablished = false;

    //Create a STOP_ROSPEC message and send it to the reader
    STOP_ROSPEC stopROSpec = new STOP_ROSPEC();
    stopROSpec.setROSpecID(new UnsignedInteger(Server.ROSPEC_ID));
    Send(stopROSpec);
   
    //Create a DISABLE_ROSPEC message and send it to the reader
    DISABLE_ROSPEC disableROSpec = new DISABLE_ROSPEC();
    disableROSpec.setROSpecID(new UnsignedInteger(Server.ROSPEC_ID));
    Send(disableROSpec);
   
    //Create a DELETE_ROSPEC message and send it to the reader
    DELETE_ROSPEC deleteROSpec = new DELETE_ROSPEC();
    deleteROSpec.setROSpecID(new UnsignedInteger(Server.ROSPEC_ID));
    Send(deleteROSpec);
   
    //wait before closing the connection
    ToolBox.Pause(ServerRuntimeProperties.Property.PauseAfterSendMillis.GetAsLong());
   
    // Create a CLOSE_CONNECTION message and send it to the reader
    CLOSE_CONNECTION closeConnection = new CLOSE_CONNECTION();
    Send(closeConnection);
  }
  
  public static void main(String[] args)
  {
    try
    {
      BasicConfigurator.configure();
      Server server = new Server();
      server.Start();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

}
