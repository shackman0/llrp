package com.databright.llrp.server;

import com.databright.llrp.AbstractEventDrivenEndpoint;

/**
 * <p>Title: ServerEndpoint</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public class ServerEndpoint
  extends AbstractEventDrivenEndpoint
{
  

  public ServerEndpoint()
  {
    super();
  }

}
