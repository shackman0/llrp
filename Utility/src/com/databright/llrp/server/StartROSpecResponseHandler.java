package com.databright.llrp.server;

import org.llrp.ltk.generated.messages.CLOSE_CONNECTION;
import org.llrp.ltk.generated.messages.DELETE_ROSPEC;
import org.llrp.ltk.generated.messages.DISABLE_ROSPEC;
import org.llrp.ltk.generated.messages.START_ROSPEC_RESPONSE;
import org.llrp.ltk.generated.messages.STOP_ROSPEC;
import org.llrp.ltk.types.UnsignedInteger;

import com.redskyconsulting.utility.ToolBox;




/**
 * <p>Title: StartROSpecResponseHandler</p>
 * <p>Description: unused now that we have a control panel</p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public class StartROSpecResponseHandler
  implements Runnable
{

  protected Server                _server;
  protected START_ROSPEC_RESPONSE _startROSpecResponse;
  

  private StartROSpecResponseHandler(
    Server                 server_, 
    START_ROSPEC_RESPONSE  startROSpecResponse_)
  {
    super();
    
    _server = server_;
    _startROSpecResponse = startROSpecResponse_;
  }

  @Override
  public void run()
  {
    ToolBox.Log("\n***** ready to read tags *****\n");
    ToolBox.Pause(60000);
    ToolBox.Log("\n***** done reading tags *****\n");
   
    //Create a STOP_ROSPEC message and send it to the reader
    STOP_ROSPEC stopROSpec = new STOP_ROSPEC();
    stopROSpec.setROSpecID(new UnsignedInteger(Server.ROSPEC_ID));
    _server.Send(stopROSpec);
   
    //Create a DISABLE_ROSPEC message and send it to the reader
    DISABLE_ROSPEC disableROSpec = new DISABLE_ROSPEC();
    disableROSpec.setROSpecID(new UnsignedInteger(Server.ROSPEC_ID));
    _server.Send(disableROSpec);
   
    //Create a DELETE_ROSPEC message and send it to the reader
    DELETE_ROSPEC deleteROSpec = new DELETE_ROSPEC();
    deleteROSpec.setROSpecID(new UnsignedInteger(Server.ROSPEC_ID));
    _server.Send(deleteROSpec);
   
    //wait before closing the connection
    ToolBox.Pause(ServerRuntimeProperties.Property.PauseAfterSendMillis.GetAsLong());
   
    // Create a CLOSE_CONNECTION message and send it to the reader
    CLOSE_CONNECTION cc = new CLOSE_CONNECTION();
    _server.Send(cc);
    
    System.exit(0);
  }
  
}
