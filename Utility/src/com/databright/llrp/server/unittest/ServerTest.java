package com.databright.llrp.server.unittest;

import org.apache.log4j.BasicConfigurator;
import org.llrp.ltk.generated.enumerations.ROSpecEventType;
import org.llrp.ltk.generated.messages.READER_EVENT_NOTIFICATION;
import org.llrp.ltk.generated.parameters.GPIEvent;
import org.llrp.ltk.generated.parameters.HoppingEvent;
import org.llrp.ltk.generated.parameters.ROSpecEvent;
import org.llrp.ltk.generated.parameters.ReaderEventNotificationData;
import org.llrp.ltk.generated.parameters.UTCTimestamp;
import org.llrp.ltk.net.LLRPConnectionAttemptFailedException;
import org.llrp.ltk.types.Bit;
import org.llrp.ltk.types.UnsignedInteger;
import org.llrp.ltk.types.UnsignedLong_DATETIME;
import org.llrp.ltk.types.UnsignedShort;

import com.databright.llrp.server.Server;
import com.databright.llrp.server.ServerEndpoint;
import com.databright.llrp.server.ServerRuntimeProperties;
import com.redskyconsulting.unittest.AbstractUnitTest;
import com.redskyconsulting.utility.ToolBox;

/**
 * <p>
 * Title: ServerTest
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2010
 * </p>
 * <p>
 * Company: Databright Management Systems.
 * </p>
 * 
 * @author Floyd Shackelford
 */
public class ServerTest
  extends AbstractUnitTest
{

  protected Server _server;

  public ServerTest()
  {
    super();
  }

  @Override
  protected void Prologue()
    throws LLRPConnectionAttemptFailedException
  {
    ToolBox.Log("Entering");
    try
    {
      _server = new Server();
      _server.Start();
    }
    finally
    {
      ToolBox.Log("Exiting");
    }
  }

  @Override
  protected void DoTest()
  {
    ToolBox.Log("Entering");
    try
    {
      ToolBox.Log("Server port = " + _server.GetPort());

      ServerEndpoint serverEndpoint = _server.GetEndpoint();

      // test: the error should pass through the endpoint to the endpoint event handler
      {
        serverEndpoint.errorOccured("test error message - should make it to the endpoint event handler");
      }

      // test: the error should be ignored
      {
        serverEndpoint.SetProcessMessages(false);
        serverEndpoint.errorOccured("test error message - should be ignored");
        serverEndpoint.SetProcessMessages(true);
      }

      // test: the message should pass through the endpoint to the endpoint event handler
      {
        READER_EVENT_NOTIFICATION readerEventNotificationMessage = CreateReaderEventNotification();
        serverEndpoint.messageReceived(readerEventNotificationMessage);
      }

      // test: the message should be ignored
      {
        serverEndpoint.SetProcessMessages(false);
        READER_EVENT_NOTIFICATION readerEventNotificationMessage = CreateReaderEventNotification();
        serverEndpoint.messageReceived(readerEventNotificationMessage);
        serverEndpoint.SetProcessMessages(true);
      }
    }
    finally
    {
      ToolBox.Log("Exiting");
    }
  }

  @Override
  protected void Epilogue()
  {
    ToolBox.Log("Entering");
    try
    {
      _server.Stop();
    }
    finally
    {
      ToolBox.Log("Exiting");
    }
  }


  private READER_EVENT_NOTIFICATION CreateReaderEventNotification()
  {
    READER_EVENT_NOTIFICATION readerEventNotificationMessage = new READER_EVENT_NOTIFICATION();
    
    ReaderEventNotificationData readerEventNotificationData = new ReaderEventNotificationData();
    readerEventNotificationMessage.setReaderEventNotificationData(readerEventNotificationData);
    
    UTCTimestamp timestamp = new UTCTimestamp();
    readerEventNotificationData.setTimestamp(timestamp);
    timestamp.setMicroseconds(new UnsignedLong_DATETIME(100000L));
    
    HoppingEvent hoppingEvent = new HoppingEvent();
    readerEventNotificationData.setHoppingEvent(hoppingEvent);
    hoppingEvent.setHopTableID(new UnsignedShort(1));
    hoppingEvent.setNextChannelIndex(new UnsignedShort(1));

    GPIEvent gpiEvent = new GPIEvent();
    readerEventNotificationData.setGPIEvent(gpiEvent);
    gpiEvent.setGPIEvent(new Bit(1));
    gpiEvent.setGPIPortNumber(new UnsignedShort(1));
    
    ROSpecEvent roSpecEvent = new ROSpecEvent();
    readerEventNotificationData.setROSpecEvent(roSpecEvent);
    roSpecEvent.setROSpecID(new UnsignedInteger(1));
    roSpecEvent.setPreemptingROSpecID(new UnsignedInteger(1));
    
    ROSpecEventType roSpecEventType = new ROSpecEventType();
    roSpecEvent.setEventType(roSpecEventType);
    roSpecEventType.set(1);
    
    return readerEventNotificationMessage;
  }
  
  
  public static void main(String[] args)
  {
    BasicConfigurator.configure();
    ServerRuntimeProperties.GetInstance();
    
    ServerTest serverTest = new ServerTest();
    serverTest.Test();
  }

}
