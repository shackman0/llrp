package com.databright.llrp.server;

import java.net.InetAddress;
import java.util.List;

import org.llrp.ltk.generated.enumerations.AISpecStopTriggerType;
import org.llrp.ltk.generated.enumerations.AccessReportTriggerType;
import org.llrp.ltk.generated.enumerations.AirProtocols;
import org.llrp.ltk.generated.enumerations.GetReaderConfigRequestedData;
import org.llrp.ltk.generated.enumerations.NotificationEventType;
import org.llrp.ltk.generated.enumerations.ROReportTriggerType;
import org.llrp.ltk.generated.enumerations.ROSpecStartTriggerType;
import org.llrp.ltk.generated.enumerations.ROSpecState;
import org.llrp.ltk.generated.enumerations.ROSpecStopTriggerType;
import org.llrp.ltk.generated.messages.ADD_ROSPEC;
import org.llrp.ltk.generated.messages.ENABLE_ROSPEC;
import org.llrp.ltk.generated.messages.GET_READER_CONFIG;
import org.llrp.ltk.generated.messages.READER_EVENT_NOTIFICATION;
import org.llrp.ltk.generated.messages.SET_READER_CONFIG;
import org.llrp.ltk.generated.messages.START_ROSPEC;
import org.llrp.ltk.generated.parameters.AISpec;
import org.llrp.ltk.generated.parameters.AISpecEvent;
import org.llrp.ltk.generated.parameters.AISpecStopTrigger;
import org.llrp.ltk.generated.parameters.AccessReportSpec;
import org.llrp.ltk.generated.parameters.AntennaEvent;
import org.llrp.ltk.generated.parameters.C1G2EPCMemorySelector;
import org.llrp.ltk.generated.parameters.ConnectionAttemptEvent;
import org.llrp.ltk.generated.parameters.ConnectionCloseEvent;
import org.llrp.ltk.generated.parameters.Custom;
import org.llrp.ltk.generated.parameters.EventNotificationState;
import org.llrp.ltk.generated.parameters.GPIEvent;
import org.llrp.ltk.generated.parameters.HoppingEvent;
import org.llrp.ltk.generated.parameters.InventoryParameterSpec;
import org.llrp.ltk.generated.parameters.RFSurveyEvent;
import org.llrp.ltk.generated.parameters.ROBoundarySpec;
import org.llrp.ltk.generated.parameters.ROReportSpec;
import org.llrp.ltk.generated.parameters.ROSpec;
import org.llrp.ltk.generated.parameters.ROSpecEvent;
import org.llrp.ltk.generated.parameters.ROSpecStartTrigger;
import org.llrp.ltk.generated.parameters.ROSpecStopTrigger;
import org.llrp.ltk.generated.parameters.ReaderEventNotificationData;
import org.llrp.ltk.generated.parameters.ReaderEventNotificationSpec;
import org.llrp.ltk.generated.parameters.ReaderExceptionEvent;
import org.llrp.ltk.generated.parameters.ReportBufferLevelWarningEvent;
import org.llrp.ltk.generated.parameters.ReportBufferOverflowErrorEvent;
import org.llrp.ltk.generated.parameters.TagReportContentSelector;
import org.llrp.ltk.types.Bit;
import org.llrp.ltk.types.UnsignedByte;
import org.llrp.ltk.types.UnsignedInteger;
import org.llrp.ltk.types.UnsignedShort;
import org.llrp.ltk.types.UnsignedShortArray;

import com.databright.llrp.LLRPIOHandler;
import com.redskyconsulting.utility.ToolBox;



/**
 * <p>Title: ReaderEventNotificationHandler</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public class ReaderEventNotificationHandler
  implements Runnable
{

  protected Server                    _server;
  protected READER_EVENT_NOTIFICATION _readerEventNotification;

  
  
  public ReaderEventNotificationHandler(
    Server                    server_,
    READER_EVENT_NOTIFICATION readerEventNotification_ )
  {
    super();

    _server = server_;
    _readerEventNotification = readerEventNotification_;
  }

  @Override
  public void run()
  {
    ReaderEventNotificationData readerEventNotificationData = _readerEventNotification.getReaderEventNotificationData();

    AISpecEvent aiSpecEvent = readerEventNotificationData.getAISpecEvent();
    if (aiSpecEvent != null)
    {
      ProcessAISpecEvent(aiSpecEvent);
    }

    AntennaEvent antennaEvent = readerEventNotificationData.getAntennaEvent();
    if (antennaEvent != null)
    {
      ProcessAntennaEvent(antennaEvent);
    }

    ConnectionAttemptEvent connectionAttemptEvent = readerEventNotificationData.getConnectionAttemptEvent();
    if (connectionAttemptEvent != null)
    {
      ProcessConnectionAttemptEvent(connectionAttemptEvent);
    }

    ConnectionCloseEvent connectionCloseEvent = readerEventNotificationData.getConnectionCloseEvent();
    if (connectionCloseEvent != null)
    {
      ProcessConnectionCloseEvent(connectionCloseEvent);
    }

    List<Custom> customList = readerEventNotificationData.getCustomList();
    if (customList != null)
    {
      ProcessCustomList(customList);
    }

    GPIEvent gpiEvent = readerEventNotificationData.getGPIEvent();
    if (gpiEvent != null)
    {
      ProcessGPIEvent(gpiEvent);
    }

    HoppingEvent hoppingEvent = readerEventNotificationData.getHoppingEvent();
    if (hoppingEvent != null)
    {
      ProcessHoppingEvent(hoppingEvent);
    }

    ReaderExceptionEvent readerExceptionEvent = readerEventNotificationData.getReaderExceptionEvent();
    if (readerExceptionEvent != null)
    {
      ProcessReaderExceptionEvent(readerExceptionEvent);
    }

    ReportBufferLevelWarningEvent reportBufferLevelWarningEvent = readerEventNotificationData.getReportBufferLevelWarningEvent();
    if (reportBufferLevelWarningEvent != null)
    {
      ProcessReportBufferLevelWarningEvent(reportBufferLevelWarningEvent);
    }

    ReportBufferOverflowErrorEvent reportBufferOverflowErrorEvent = readerEventNotificationData.getReportBufferOverflowErrorEvent();
    if (reportBufferOverflowErrorEvent != null)
    {
      ProcessReportBufferOverflowErrorEvent(reportBufferOverflowErrorEvent);
    }

    RFSurveyEvent rfSurveyEvent = readerEventNotificationData.getRFSurveyEvent();
    if (rfSurveyEvent != null)
    {
      ProcessRFSurveyEvent(rfSurveyEvent);
    }

    ROSpecEvent roSpecEvent = readerEventNotificationData.getROSpecEvent();
    if (roSpecEvent != null)
    {
      ProcessROSpecEvent(roSpecEvent);
    }

    // we have to do our own clean up since this runs in a thread separate from the LLRPIOHandler
    LLRPIOHandler.RemoveInetAddress(_readerEventNotification);
  }


  
  protected void ProcessAISpecEvent(AISpecEvent aiSpecEvent_)
  {
    // todo
    ToolBox.Log("ProcessAISpecEvent not implemented yet");
  }

  protected void ProcessAntennaEvent(AntennaEvent antennaEvent_)
  {
    // todo
    ToolBox.Log("ProcessAntennaEvent not implemented yet");
  }

  protected void ProcessConnectionAttemptEvent(ConnectionAttemptEvent connectionAttemptEvent_)
  {
    InetAddress inetAddress = LLRPIOHandler.GetInetAddress(_readerEventNotification);
    if (inetAddress != null)
    {
      String deviceIdentifier = LLRPIOHandler.GetDeviceIdentifer(inetAddress);
      if (deviceIdentifier == null)
      {
        GET_READER_CONFIG getReaderConfig = new GET_READER_CONFIG();
        getReaderConfig.setAntennaID(new UnsignedShort(0));
        getReaderConfig.setGPIPortNum(new UnsignedShort(0));
        getReaderConfig.setGPOPortNum(new UnsignedShort(0));
        GetReaderConfigRequestedData readerConfigRequestedData = new GetReaderConfigRequestedData();
        getReaderConfig.setRequestedData(readerConfigRequestedData);
        readerConfigRequestedData.set(GetReaderConfigRequestedData.Identification);
        _server.Send(getReaderConfig);
      }
    }
    
    // Create a SET_READER_CONFIG Message and send it to the reader
    SET_READER_CONFIG setReaderConfig = CreateSetReaderConfig();
    _server.Send(setReaderConfig);
    
    // CREATE an ADD_ROSPEC Message and send it to the reader
    ADD_ROSPEC addROSpec = new ADD_ROSPEC();
    addROSpec.setROSpec(CreateROSpec());
    _server.Send(addROSpec);

    // Create an ENABLE_ROSPEC message and send it to the reader
    ENABLE_ROSPEC enableROSpec = new ENABLE_ROSPEC();
    enableROSpec.setROSpecID(new UnsignedInteger(Server.ROSPEC_ID));
    _server.Send(enableROSpec);

    // Create a START_ROSPEC message and send it to the reader
    START_ROSPEC startROSpec = new START_ROSPEC();
    startROSpec.setROSpecID(new UnsignedInteger(Server.ROSPEC_ID));
    _server.Send(startROSpec);
    
    // the START_ROSPEC_RESPONSE will cause a READER_EVENT_NOTIFICATION.ROSpecEvent.Start_Of_ROSpec 
    // to be sent to the server.
  }

  protected void ProcessConnectionCloseEvent(ConnectionCloseEvent connectionCloseEvent_)
  {
    // todo
    ToolBox.Log("ProcessConnectionCloseEvent not implemented yet");
  }

  protected void ProcessCustomList(List<Custom> customList_)
  {
    // todo
    ToolBox.Log("ProcessCustomList not implemented yet");
  }

  protected void ProcessGPIEvent(GPIEvent gpiEvent_)
  {
    // todo
    ToolBox.Log("ProcessGPIEvent not implemented yet");
  }

  protected void ProcessHoppingEvent(HoppingEvent hoppingEvent_)
  {
    // todo
    ToolBox.Log("ProcessHoppingEvent not implemented yet");
  }

  protected void ProcessReaderExceptionEvent(ReaderExceptionEvent readerExceptionEvent_)
  {
    // todo
    ToolBox.Log("ProcessReaderExceptionEvent not implemented yet");
  }

  protected void ProcessReportBufferLevelWarningEvent(ReportBufferLevelWarningEvent reportBufferLevelWarningEvent_)
  {
    // todo
    ToolBox.Log("ProcessReportBufferLevelWarningEvent not implemented yet");
  }

  protected void ProcessReportBufferOverflowErrorEvent(ReportBufferOverflowErrorEvent reportBufferOverflowErrorEvent_)
  {
    // todo
    ToolBox.Log("ProcessReportBufferOverflowErrorEvent not implemented yet");
  }

  protected void ProcessRFSurveyEvent(RFSurveyEvent rfSurveyEvent_)
  {
    // todo
    ToolBox.Log("ProcessRFSurveyEvent not implemented yet");
  }

  protected void ProcessROSpecEvent(ROSpecEvent roSpecEvent_)
  {
    new ROSpecEventHandler(_server, roSpecEvent_).run();
  }

  /**
   * This method creates a SET_READER_CONFIG method
   * 
   * @return
   */
  private SET_READER_CONFIG CreateSetReaderConfig()
  {
    SET_READER_CONFIG setReaderConfig = new SET_READER_CONFIG();

    ROReportSpec roReportSpec = new ROReportSpec();
    roReportSpec.setN(new UnsignedShort(1));  // RO reports are to be sent as soon one tag is read
    roReportSpec.setROReportTrigger(new ROReportTriggerType(ROReportTriggerType.Upon_N_Tags_Or_End_Of_ROSpec));
    TagReportContentSelector tagReportContentSelector = new TagReportContentSelector();
    tagReportContentSelector.setEnableAccessSpecID(new Bit(1));
    tagReportContentSelector.setEnableAntennaID(new Bit(1));
    tagReportContentSelector.setEnableChannelIndex(new Bit(1));
    tagReportContentSelector.setEnableFirstSeenTimestamp(new Bit(1));
    tagReportContentSelector.setEnableInventoryParameterSpecID(new Bit(1));
    tagReportContentSelector.setEnableLastSeenTimestamp(new Bit(1));
    tagReportContentSelector.setEnablePeakRSSI(new Bit(1));
    tagReportContentSelector.setEnableROSpecID(new Bit(1));
    tagReportContentSelector.setEnableSpecIndex(new Bit(1));
    tagReportContentSelector.setEnableTagSeenCount(new Bit(1));
    
    C1G2EPCMemorySelector epcMemSel = new C1G2EPCMemorySelector();
    epcMemSel.setEnableCRC(new Bit(0));
    epcMemSel.setEnablePCBits(new Bit(0));
    tagReportContentSelector.addToAirProtocolEPCMemorySelectorList(epcMemSel);
    roReportSpec.setTagReportContentSelector(tagReportContentSelector);
    setReaderConfig.setROReportSpec(roReportSpec);

    // Set default AccessReportSpec
    AccessReportSpec accessReportSpec = new AccessReportSpec();
    accessReportSpec.setAccessReportTrigger(new AccessReportTriggerType(AccessReportTriggerType.Whenever_ROReport_Is_Generated));
    setReaderConfig.setAccessReportSpec(accessReportSpec);

    // Set up reporting for AISpec events, ROSpec events, and GPI Events
    ReaderEventNotificationSpec eventNoteSpec = new ReaderEventNotificationSpec();
    EventNotificationState noteState = new EventNotificationState();
    noteState.setEventType(new NotificationEventType(NotificationEventType.AISpec_Event));
    noteState.setNotificationState(new Bit(0));
    eventNoteSpec.addToEventNotificationStateList(noteState);
    noteState = new EventNotificationState();
    noteState.setEventType(new NotificationEventType(NotificationEventType.ROSpec_Event));
    noteState.setNotificationState(new Bit(1));
    eventNoteSpec.addToEventNotificationStateList(noteState);
    noteState = new EventNotificationState();
    noteState.setEventType(new NotificationEventType(NotificationEventType.GPI_Event));
    noteState.setNotificationState(new Bit(0));
    eventNoteSpec.addToEventNotificationStateList(noteState);
    setReaderConfig.setReaderEventNotificationSpec(eventNoteSpec);

    setReaderConfig.setResetToFactoryDefault(new Bit(0));

    return setReaderConfig;
  }

  /**
   * This method creates a ROSpec with null start and stop triggers
   * 
   * @return
   */
  protected ROSpec CreateROSpec()
  {

    // create a new rospec
    ROSpec roSpec = new ROSpec();
    roSpec.setPriority(new UnsignedByte(0));
    roSpec.setCurrentState(new ROSpecState(ROSpecState.Disabled));
    roSpec.setROSpecID(new UnsignedInteger(Server.ROSPEC_ID));

    // set up ROBoundary (start and stop triggers)
    ROBoundarySpec roBoundarySpec = new ROBoundarySpec();

    ROSpecStartTrigger startTrig = new ROSpecStartTrigger();
    startTrig.setROSpecStartTriggerType(new ROSpecStartTriggerType(ROSpecStartTriggerType.Null));
    roBoundarySpec.setROSpecStartTrigger(startTrig);

    ROSpecStopTrigger stopTrig = new ROSpecStopTrigger();
    stopTrig.setDurationTriggerValue(new UnsignedInteger(0));
    stopTrig.setROSpecStopTriggerType(new ROSpecStopTriggerType(ROSpecStopTriggerType.Null));
    roBoundarySpec.setROSpecStopTrigger(stopTrig);

    roSpec.setROBoundarySpec(roBoundarySpec);

    // Add an AISpec
    AISpec aispec = new AISpec();

    // set AI Stop trigger to null
    AISpecStopTrigger aiStopTrigger = new AISpecStopTrigger();
    aiStopTrigger.setAISpecStopTriggerType(new AISpecStopTriggerType(AISpecStopTriggerType.Null));
    aiStopTrigger.setDurationTrigger(new UnsignedInteger(0));
    aispec.setAISpecStopTrigger(aiStopTrigger);

    UnsignedShortArray antennaIDs = new UnsignedShortArray();
    antennaIDs.add(new UnsignedShort(0));
    aispec.setAntennaIDs(antennaIDs);

    InventoryParameterSpec inventoryParam = new InventoryParameterSpec();
    inventoryParam.setProtocolID(new AirProtocols(AirProtocols.EPCGlobalClass1Gen2));
    inventoryParam.setInventoryParameterSpecID(new UnsignedShort(1));
    aispec.addToInventoryParameterSpecList(inventoryParam);

    roSpec.addToSpecParameterList(aispec);

    return roSpec;
  }

}
