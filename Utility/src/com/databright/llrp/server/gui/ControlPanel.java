package com.databright.llrp.server.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import org.apache.log4j.BasicConfigurator;
import org.llrp.ltk.net.LLRPConnectionAttemptFailedException;

import com.databright.llrp.server.Server;
import com.databright.llrp.server.ServerRuntimeProperties;
import com.redskyconsulting.utility.InfoWindow;
import com.redskyconsulting.utility.MultiLineLabelUI;
import com.redskyconsulting.utility.MultiLineToolTipUI;
import com.redskyconsulting.utility.ToolBox;




/**
 * <p>Title: ControlPanel</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public class ControlPanel
  extends JFrame
  implements WindowListener
{

  public static ControlPanel __controlPanel;
  
  private static final long serialVersionUID = 1L;

  protected Insets ZERO_INSETS = new Insets(0,0,0,0);

  
  protected JButton       _startServer;
  protected JButton       _stopServer;
  protected JButton       _showConsole;
  protected JButton       _reloadProperties;

  protected Server        _server;

  
  
  public ControlPanel()
  {
    super();
  }
  
  public void Initialize()
  {
    MultiLineToolTipUI.initialize();
    MultiLineLabelUI.initialize();

    setTitle("Databright LLRP Server");
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    addWindowListener(this);

    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    JPanel northPanel = new JPanel();
    contentPane.add(northPanel,BorderLayout.NORTH);
    northPanel.setLayout(new GridBagLayout());
    
    JPanel eastPanel = new JPanel();
    contentPane.add(eastPanel,BorderLayout.EAST);
    eastPanel.setLayout(new GridBagLayout());
    
    JPanel westPanel = new JPanel();
    contentPane.add(westPanel,BorderLayout.WEST);
    westPanel.setLayout(new GridBagLayout());
    
    JPanel centerPanel = new JPanel();
    contentPane.add(centerPanel,BorderLayout.CENTER);
    centerPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
    
    JPanel southPanel = new JPanel();
    contentPane.add(southPanel,BorderLayout.SOUTH);
    southPanel.setLayout(new GridBagLayout());

    
    _startServer = new JButton("start server");
    centerPanel.add(_startServer);
    _startServer.addActionListener(new StartServerActionHandler());
    
    _stopServer = new JButton("stop server");
    centerPanel.add(_stopServer);
    _stopServer.addActionListener(new StopServerActionHandler());
    _stopServer.setEnabled(false);
    
    _showConsole = new JButton("show console");
    centerPanel.add(_showConsole);
    _showConsole.addActionListener(new ShowConsoleActionHandler());
    
    _reloadProperties = new JButton("reload properties");
    centerPanel.add(_reloadProperties);
    _reloadProperties.addActionListener(new ReloadPropertiesActionHandler());
    
    pack();

    ToolBox.SetLocation2CenterOfScreen(this);
    
    __controlPanel = this;
  }


  @Override
  public void windowActivated(WindowEvent windowEvent_)
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void windowClosed(WindowEvent windowEvent_)
  {
    // TODO Auto-generated method stub
  }

  @Override
  public void windowClosing(WindowEvent windowEvent_)
  {
    StopServer();

    dispose();
    System.exit(0);
  }

  @Override
  public void windowDeactivated(WindowEvent windowEvent_)
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void windowDeiconified(WindowEvent windowEvent_)
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void windowIconified(WindowEvent windowEvent_)
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void windowOpened(WindowEvent windowEvent_)
  {
    // TODO Auto-generated method stub
    
  }

  

  
  public static void main(String... args)
  {
    BasicConfigurator.configure();

    ControlPanel controlPanel = new ControlPanel();
    controlPanel.Initialize();
    controlPanel.setVisible(true);
  }

  

  protected void StartServer()
  {
    _startServer.setEnabled(false);
    _stopServer.setEnabled(true);

    _server = new Server();
    try
    {
      _server.Start();
    }
    catch (LLRPConnectionAttemptFailedException ex)
    {
      _startServer.setEnabled(true);
      _stopServer.setEnabled(false);

      ToolBox.Log(ex);
    }
  }
  
  class StartServerActionHandler
    implements ActionListener
  {
    public StartServerActionHandler()
    { 
      super();
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent_)
    {
      StartServer();
    }
  }

  protected void StopServer()
  {
    _startServer.setEnabled(true);
    _stopServer.setEnabled(false);
    
    if (_server != null)
    {
      _server.Stop();
      _server = null;
    }
  }
  
  class StopServerActionHandler
    implements ActionListener
  {
    public StopServerActionHandler()
    { 
      super();
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent_)
    {
      StopServer();
    }
  }
  
  protected void ShowConsole()
  {
    InfoWindow.Show();
  }
  
  class ShowConsoleActionHandler
    implements ActionListener
  {
    public ShowConsoleActionHandler()
    { 
      super();
    }
  
    @Override
    public void actionPerformed(ActionEvent actionEvent_)
    {
      ShowConsole();
    }
    
  }
  
  protected void ReloadProperties()
  {
    ServerRuntimeProperties.GetInstance().Load();
    ToolBox.Log("Properties reloaded\n" + ServerRuntimeProperties.GetInstance());
  }
  
  class ReloadPropertiesActionHandler
    implements ActionListener
  {
    public ReloadPropertiesActionHandler()
    { 
      super();
    }
  
    @Override
    public void actionPerformed(ActionEvent actionEvent_)
    {
      ReloadProperties();
    }
    
  }

  
}
