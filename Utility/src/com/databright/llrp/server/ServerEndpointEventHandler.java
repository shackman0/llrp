package com.databright.llrp.server;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import org.llrp.ltk.generated.messages.ADD_ACCESSSPEC_RESPONSE;
import org.llrp.ltk.generated.messages.ADD_ROSPEC_RESPONSE;
import org.llrp.ltk.generated.messages.CLOSE_CONNECTION_RESPONSE;
import org.llrp.ltk.generated.messages.CUSTOM_MESSAGE;
import org.llrp.ltk.generated.messages.DELETE_ACCESSSPEC_RESPONSE;
import org.llrp.ltk.generated.messages.DELETE_ROSPEC_RESPONSE;
import org.llrp.ltk.generated.messages.DISABLE_ACCESSSPEC_RESPONSE;
import org.llrp.ltk.generated.messages.DISABLE_ROSPEC_RESPONSE;
import org.llrp.ltk.generated.messages.ENABLE_ACCESSSPEC_RESPONSE;
import org.llrp.ltk.generated.messages.ENABLE_ROSPEC_RESPONSE;
import org.llrp.ltk.generated.messages.ERROR_MESSAGE;
import org.llrp.ltk.generated.messages.GET_ACCESSSPECS_RESPONSE;
import org.llrp.ltk.generated.messages.GET_READER_CAPABILITIES_RESPONSE;
import org.llrp.ltk.generated.messages.GET_READER_CONFIG_RESPONSE;
import org.llrp.ltk.generated.messages.GET_ROSPECS_RESPONSE;
import org.llrp.ltk.generated.messages.KEEPALIVE;
import org.llrp.ltk.generated.messages.KEEPALIVE_ACK;
import org.llrp.ltk.generated.messages.READER_EVENT_NOTIFICATION;
import org.llrp.ltk.generated.messages.RO_ACCESS_REPORT;
import org.llrp.ltk.generated.messages.SET_READER_CONFIG_RESPONSE;
import org.llrp.ltk.generated.messages.START_ROSPEC_RESPONSE;
import org.llrp.ltk.generated.messages.STOP_ROSPEC_RESPONSE;
import org.llrp.ltk.generated.parameters.TagReportData;
import org.llrp.ltk.types.LLRPMessage;

import com.databright.llrp.LLRPEndpointEvent;
import com.databright.llrp.LLRPEndpointListener;
import com.databright.llrp.LLRPIOHandler;
import com.databright.llrp.Sync2HelixHandler;
import com.databright.llrp.TagReportInformation;
import com.redskyconsulting.utility.ToolBox;

/**
 * <p>
 * Title: ServerEndpointEventHandler
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2010
 * </p>
 * <p>
 * Company: Databright Management Systems.
 * </p>
 * 
 * @author Floyd Shackelford
 */
public class ServerEndpointEventHandler
  implements LLRPEndpointListener<ServerEndpoint>
{

  protected Server     _server;

  
  
  public ServerEndpointEventHandler(Server server_)
  {
    super();

    _server = server_;
  }

  @Override
  public void LLRPErrorOccurred(LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_)
  {
    String errorMessage = String.valueOf(llrpMessageReceivedEvent_.GetOther());
    ToolBox.Log(errorMessage);
  }

  @Override
  public void LLRPMessageReceived(LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_)
  {
    LLRPMessage llrpMessage = llrpMessageReceivedEvent_.GetLLRPMessage();
//    ToolBox.Log(llrpMessage.getClass().getSimpleName());

    try
    {
      Class llrpMessageClass = llrpMessage.getClass();
      String llrpMessageClassSimpleName = llrpMessageClass.getSimpleName();
      Method method = getClass().getDeclaredMethod(llrpMessageClassSimpleName, LLRPEndpointEvent.class, llrpMessageClass);
      method.invoke(this, llrpMessageReceivedEvent_, llrpMessage);
    }
    catch (SecurityException ex)
    {
      ex.printStackTrace();
    }
    catch (NoSuchMethodException ex)
    {
      ex.printStackTrace();
    }
    catch (IllegalArgumentException ex)
    {
      ex.printStackTrace();
    }
    catch (IllegalAccessException ex)
    {
      ex.printStackTrace();
    }
    catch (InvocationTargetException ex)
    {
      ex.printStackTrace();
    }
  }

  protected void READER_EVENT_NOTIFICATION(LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_, READER_EVENT_NOTIFICATION readerEventNotification_)
  {
    Thread thread = new Thread(new ReaderEventNotificationHandler(_server,readerEventNotification_));
    thread.start();
  }

  protected void KEEPALIVE(LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_, KEEPALIVE keepalive_)
  {
    KEEPALIVE_ACK keepAliveAck = new KEEPALIVE_ACK();
    _server.Send(keepAliveAck);
  }

  protected void ADD_ACCESSSPEC_RESPONSE(LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_, ADD_ACCESSSPEC_RESPONSE addAccessSpecResponse_)
  {
    // todo
  }

  protected void ADD_ROSPEC_RESPONSE(LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_, ADD_ROSPEC_RESPONSE addROSpecResponse_)
  {
    // todo
  }

  protected void CLOSE_CONNECTION_RESPONSE(LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_, CLOSE_CONNECTION_RESPONSE closeConnectionResponse_)
  {
    LLRPIOHandler.RemoveDeviceIdentifier(closeConnectionResponse_);
  }

  protected void CUSTOM_MESSAGE(LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_, CUSTOM_MESSAGE customMessage_)
  {
    // todo
  }

  protected void DELETE_ACCESSSPEC_RESPONSE(LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_, DELETE_ACCESSSPEC_RESPONSE deleteAccessSpecResponse_)
  {
    // todo
  }

  protected void DELETE_ROSPEC_RESPONSE(LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_, DELETE_ROSPEC_RESPONSE deleteROSpecResponse_)
  {
    // todo
  }

  protected void DISABLE_ACCESSSPEC_RESPONSE(LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_, DISABLE_ACCESSSPEC_RESPONSE disableAccessSpecResponse_)
  {
    // todo
  }

  protected void DISABLE_ROSPEC_RESPONSE(LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_, DISABLE_ROSPEC_RESPONSE disableROSpecResponse_)
  {
    // todo
  }

  protected void ENABLE_ACCESSSPEC_RESPONSE(LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_, ENABLE_ACCESSSPEC_RESPONSE enableAccessSpecResponse_)
  {
    // todo
  }

  protected void ENABLE_ROSPEC_RESPONSE(LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_, ENABLE_ROSPEC_RESPONSE enableROSpecResponse_)
  {
    // todo
  }

  protected void ERROR_MESSAGE(LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_, ERROR_MESSAGE errorMessage_)
  {
    // todo
  }

  protected void GET_ACCESSSPECS_RESPONSE(LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_, GET_ACCESSSPECS_RESPONSE getAccessSpecsResponse_)
  {
    // todo
  }

  protected void GET_READER_CAPABILITIES_RESPONSE(LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_, GET_READER_CAPABILITIES_RESPONSE getReaderCapabilitiesResponse_)
  {
    // todo
  }

  protected void GET_READER_CONFIG_RESPONSE(
    LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_, 
    GET_READER_CONFIG_RESPONSE        getReaderConfigResponse_ )
  {
    LLRPIOHandler.PutDeviceIdentifier(getReaderConfigResponse_);
  }

  protected void GET_ROSPECS_RESPONSE(LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_, GET_ROSPECS_RESPONSE getROSpecResponse_)
  {
    // todo
  }


  
  protected void RO_ACCESS_REPORT(
    LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_, 
    RO_ACCESS_REPORT                  roAccessReport_ )
  {
    String deviceIdentifier = LLRPIOHandler.GetDeviceIdentifier(roAccessReport_);

    List<TagReportData> tagReportDataList = roAccessReport_.getTagReportDataList();
    for (TagReportData tagReportData: tagReportDataList)
    {
      TagReportInformation tagReportInformation = new TagReportInformation(deviceIdentifier,tagReportData);
      if (tagReportInformation.DoINeed2SyncThisTagRead2Helix() == true)
      {
        Sync2HelixHandler sync2HelixHandler = new Sync2HelixHandler(tagReportInformation,Sync2HelixHandler.EventType.NewTag);
        sync2HelixHandler.Sync2Helix();
      }
      else
      {
        ToolBox.Log("Not synching duplicate read " + tagReportInformation.CreateKeyForDeviceAndTag2TagReportInformationMap());
      }
    }
    
  }
  
  
  protected void SET_READER_CONFIG_RESPONSE(LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_, SET_READER_CONFIG_RESPONSE setReaderConfigResponse_)
  {
    // todo
  }

  protected void START_ROSPEC_RESPONSE(LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_, START_ROSPEC_RESPONSE startROSpecResponse_)
  {
    // todo: need to check that the status indicates that the rospec was started
    // todo: perhaps this should be moved to the READER_EVENT_NOTIFICATION?
//    LLRPStatus llrpStatus = startROSpecResponse_.getLLRPStatus();
//    StatusCode statusCode = llrpStatus.getStatusCode();
//    ToolBox.Log(String.valueOf(statusCode.intValue()));
//    if (statusCode.intValue() == 0)
//    {
//      
//    }
    
    _server.SetConnectionEstablished(true);

    // we no longer use this since we now have a console
//    Thread thread = new Thread(new StartROSpecResponseHandler(_server, startROSpecResponse_));
//    thread.start();
  }

  protected void STOP_ROSPEC_RESPONSE(LLRPEndpointEvent<ServerEndpoint> llrpMessageReceivedEvent_, STOP_ROSPEC_RESPONSE stopROSpecResponse_)
  {
    // todo
  }

}
