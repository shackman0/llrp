package com.databright.llrp;

import org.llrp.ltk.generated.interfaces.EPCParameter;
import org.llrp.ltk.generated.parameters.AntennaID;
import org.llrp.ltk.generated.parameters.FirstSeenTimestampUTC;
import org.llrp.ltk.generated.parameters.TagReportData;

import com.redskyconsulting.utility.GregorianCalendar;
import com.redskyconsulting.utility.HashMap;


/**
 * <p>Title: TagReportInformation</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public class TagReportInformation
{

  /*
   * once i see a tag, don't send the tag again as long as it is in this map.
   * it is removed from this map by RemoveStaleTags.
   * 
   * key = CreateKeyForDeviceAndTag2TagReportInformationMap()
   * value = TagReportInformation
   */
  public static HashMap<String,TagReportInformation>  __deviceAndTag2TagReportInformationMap;
  static
  {
    __deviceAndTag2TagReportInformationMap = new HashMap();
  }
  
  public static void PutTagReportInformation(TagReportInformation tagReportInformation)
  {
    synchronized (__deviceAndTag2TagReportInformationMap)
    {
      tagReportInformation._previousTagReportInformation = 
        __deviceAndTag2TagReportInformationMap.put(
          tagReportInformation.CreateKeyForDeviceAndTag2TagReportInformationMap(), 
          tagReportInformation );
    }
  }

  public static TagReportInformation RemoveTagReportInformation(String key)
  {
    TagReportInformation tagReportInformation;
    
    synchronized (__deviceAndTag2TagReportInformationMap)
    {
      tagReportInformation = __deviceAndTag2TagReportInformationMap.remove(key);
    }
    
    return tagReportInformation;
  }
  
  
  protected String _deviceIdentifier;
  protected String _antennaIdentifier;
  protected String _tagIdentifier;
  protected String _firstSeenDTS;

  protected GregorianCalendar _dts = GregorianCalendar.Now();
  
  /**
   * points to the immediately previous TagReportInformation having the same device-id tag-id key
   * implements a linked list of TagReportInformation.
   * the most current is stored in __deviceAndTag2TagReportInformationMap.
   * the oldest has _previousTagReportInformation = null.
   */
  protected TagReportInformation _previousTagReportInformation = null;


  public TagReportInformation(
    String deviceIdentifier_,
    String antennaIdentifier_,
    String tagIdentifier_,
    String firstSeenDTS_ )
  {
    super();
    
    _deviceIdentifier = deviceIdentifier_;
    _antennaIdentifier = antennaIdentifier_;
    _tagIdentifier = tagIdentifier_;
    _firstSeenDTS = firstSeenDTS_;
    
    PutTagReportInformation(this);
  }
  
  
  public TagReportInformation(
    String        deviceIdentifier_,
    TagReportData tagReportData_ )
  {
    super();

    _deviceIdentifier = deviceIdentifier_;

    AntennaID antennaId = tagReportData_.getAntennaID();
    String rawAntennaId = antennaId.toString();
    _antennaIdentifier = rawAntennaId.substring(rawAntennaId.lastIndexOf(" ") + 1); 

    EPCParameter epcParameter = tagReportData_.getEPCParameter();  // aka tag identifier
    String epcParameterString = epcParameter.toString().trim(); 
    _tagIdentifier = epcParameterString.substring(epcParameterString.lastIndexOf(" ") + 1); // the tag identifier is the trailing number
    
    FirstSeenTimestampUTC firstSeenTimestampUTC = tagReportData_.getFirstSeenTimestampUTC();
    String firstSeenTimestampUTCString = firstSeenTimestampUTC.toString();
    _firstSeenDTS = firstSeenTimestampUTCString.substring(firstSeenTimestampUTCString.lastIndexOf(" ") + 1); // the dts is the value
    
    PutTagReportInformation(this);
  }

  public String GetDeviceIdentifier()
  {
    return _deviceIdentifier;
  }

  public String GetAntennaIdentifier()
  {
    return _antennaIdentifier;
  }

  public String GetTagIdentifier()
  {
    return _tagIdentifier;
  }

  public String GetFirstSeenDTS()
  {
    return _firstSeenDTS;
  }

  public GregorianCalendar GetDTS()
  {
    return _dts;
  }

  public TagReportInformation GetPreviousTagReportInformation()
  {
    return _previousTagReportInformation;
  }

  public boolean DoINeed2SyncThisTagRead2Helix()
  {
    return (_previousTagReportInformation == null);
  }

  public String CreateKeyForDeviceAndTag2TagReportInformationMap()
  {
    return _deviceIdentifier + "," + _tagIdentifier;
  }
  
}
