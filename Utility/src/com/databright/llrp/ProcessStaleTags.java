package com.databright.llrp;

import java.util.Collection;

import com.databright.llrp.server.ServerRuntimeProperties;
import com.redskyconsulting.utility.GregorianCalendar;
import com.redskyconsulting.utility.ToolBox;





/**
 * <p>Title: ProcessStaleTags</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public class ProcessStaleTags
  implements Runnable
{
  
  protected boolean    _exit = false;
  
  protected boolean    _sync2Helix = true;
  
  
  public ProcessStaleTags()
  {
    super();
  }

  public void Exit()
  {
    _exit = true;
  }
  
  @Override
  public void run()
  {
    while (_exit == false)
    {
      try
      {
        Thread.sleep(ServerRuntimeProperties.Property.ProcessStaleTagsSleepMillis.GetAsLong());
      }
      catch (InterruptedException ex)
      {
        ex.printStackTrace();
      }
      
      long nowMillis = GregorianCalendar.Now().getTimeInMillis();
      
      Collection<String> keys = TagReportInformation.__deviceAndTag2TagReportInformationMap.GetSafeKeys();
      for (String key: keys)
      {
        TagReportInformation tagReportInformation = TagReportInformation.__deviceAndTag2TagReportInformationMap.get(key);
        if (tagReportInformation != null)
        {
          long tagReportInformationDTSMillis = tagReportInformation.GetDTS().getTimeInMillis();
          long millisSinceLastRead = nowMillis - tagReportInformationDTSMillis;
          if (millisSinceLastRead > ServerRuntimeProperties.Property.StaleTagThresholdMillis.GetAsLong())
          {
            ProcessStaleTag(tagReportInformation);
          }
        }
      }
    }

    // if you shut down the server too early, you could lose some "Stale Tag" notifications
    if (TagReportInformation.__deviceAndTag2TagReportInformationMap.size() == 0)
    {
      ToolBox.Log("TagReportInformation.__deviceAndTag2TagReportInformationMap is empty");
    }
    else
    {
      StringBuilder stringBuilder = new StringBuilder();
      
      stringBuilder.append("Warning: shutting down but TagReportInformation.__deviceAndTag2TagReportInformationMap has ");
      stringBuilder.append(TagReportInformation.__deviceAndTag2TagReportInformationMap.size());
      stringBuilder.append(" entries:");
      stringBuilder.append("\n");
      
      Collection<String> keys = TagReportInformation.__deviceAndTag2TagReportInformationMap.GetSafeKeys();
      for (String key: keys)
      {
        stringBuilder.append(key);
        stringBuilder.append("\n");
      }
      ToolBox.Log(stringBuilder.toString());      
    }
  }

  protected void ProcessStaleTag(TagReportInformation tagReportInformation_)
  {
    String key = tagReportInformation_.CreateKeyForDeviceAndTag2TagReportInformationMap();    
    TagReportInformation.RemoveTagReportInformation(key);
    ToolBox.Log("Removed " + key);

    if (_sync2Helix == true)
    {
      Sync2HelixHandler sync2HelixHandler = new Sync2HelixHandler(tagReportInformation_,Sync2HelixHandler.EventType.StaleTag);
      sync2HelixHandler.Sync2Helix();
    }
  }
  
  public boolean GetSync2Helix()
  {
    return _sync2Helix;
  }
  
  public void SetSync2Helix(boolean sync2Helix_)
  {
    _sync2Helix = sync2Helix_;
  }
  
}
