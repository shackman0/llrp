package com.databright.llrp;

import org.llrp.ltk.types.LLRPMessage;

import com.redskyconsulting.utility.EventNotificationManager;





/**
 * <p>Title: AbstractEventDrivenEndpoint</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public abstract class AbstractEventDrivenEndpoint
  extends AbstractEndpoint
{

  protected EventNotificationManager<LLRPEndpointEvent,LLRPEndpointListener>  _llrpEndpointEventNotificationManager;

  
  
  public AbstractEventDrivenEndpoint()
  {
    super();

    _llrpEndpointEventNotificationManager = new EventNotificationManager();
  }

  public EventNotificationManager<LLRPEndpointEvent, LLRPEndpointListener> GetLLRPEndpointEventNotificationManager()
  {
    return _llrpEndpointEventNotificationManager;
  }
  

  @Override
  public void errorOccured(String errorMessage_)
  {
    super.errorOccured(errorMessage_);
    
    if (_processMessages == true)
    {
      _llrpEndpointEventNotificationManager.NotifyListenersOfEvent(
        LLRPEndpointListener.CallbackMethods.LLRPErrorOccurred.name(), 
        new LLRPEndpointEvent(this,false,errorMessage_));
    }
  }
  
  @Override
  public void messageReceived(LLRPMessage llrpMessage_)
  {
    super.messageReceived(llrpMessage_);

    if (_processMessages == true)
    {
      _llrpEndpointEventNotificationManager.NotifyListenersOfEvent(
        LLRPEndpointListener.CallbackMethods.LLRPMessageReceived.name(), 
        new LLRPEndpointEvent(this,true,llrpMessage_));
    }
  }
  

}
