package com.databright.llrp;

import java.util.concurrent.TimeoutException;

import org.llrp.ltk.net.LLRPAcceptor;
import org.llrp.ltk.net.LLRPConnectionAttemptFailedException;
import org.llrp.ltk.types.LLRPMessage;

import com.databright.llrp.server.Server;


/**
 * <p>Title: Server</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public class Acceptor
{

  protected LLRPAcceptor        _llrpAcceptor = null;
  protected Server              _server;

  
  public Acceptor(Server server_)
  {
    super();
    
    _server = server_;
  }
  
  public LLRPAcceptor GetLLRPAcceptor()
  {
    return _llrpAcceptor;
  }

  public Server GetServer()
  {
    return _server;
  }
  
  public void Bind( 
    int              port_, 
    AbstractEndpoint endpoint_ )
    throws LLRPConnectionAttemptFailedException
  {
    _llrpAcceptor = new LLRPAcceptor(endpoint_, port_);

    LLRPIOHandler llrpIOHandler = new LLRPIOHandler(this);
    llrpIOHandler.setKeepAliveAck(true);
    llrpIOHandler.setKeepAliveForward(false);

    _llrpAcceptor.setHandler(llrpIOHandler);
    
    _llrpAcceptor.bind();
  }

  public void Send(LLRPMessage llrpMessage)
  {
    _llrpAcceptor.send(llrpMessage);
  }

  public LLRPMessage Transact(LLRPMessage llrpMessage) 
    throws TimeoutException
  {
    return _llrpAcceptor.transact(llrpMessage);
  }
  
  public void Close()
  {
    _llrpAcceptor.close();
    _llrpAcceptor = null;
  }
  
  
}
