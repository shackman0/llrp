package com.redskyconsulting.unittest;

import com.redskyconsulting.utility.ToolBox;

/**
 * <p>Title: AbstractTest</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public abstract class AbstractUnitTest
{
  

  
  public AbstractUnitTest()
  {
    super();
  }
  
  public void Test()
  {
    ToolBox.Log("Entering");
    try
    {
      Prologue();
      try
      {
        DoTest();
      }
      finally
      {
        Epilogue();
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      ToolBox.Log("Exiting");
    }
  }

  protected abstract void Prologue()
    throws Exception;
  
  protected abstract void DoTest()
    throws Exception;
  
  protected abstract void Epilogue()
    throws Exception;
  
}
