package com.redskyconsulting.utility;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;




/**
 * <p>Title: HashMap</p>
 * <p>Description: HashMap</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: </p>
 * @author Floyd Shackelford
 * @version $Header: p:/cvs repository/databright/Utility/src/com/redskyconsulting/utility/HashMap.java,v 1.2 2010/07/22 14:06:43 noyb Exp $
 */

public class HashMap<Key,Value>
  extends java.util.HashMap<Key,Value>
{

  private static final long serialVersionUID = 9375100234L;
  

  protected boolean               _allowNullKey   = false;
  protected boolean               _allowNullValue = false;


  public HashMap()
  {
    super();
  }

  public HashMap (
    int   initialCapacity_,
    float loadFactor_ )
  {
    super(initialCapacity_,loadFactor_);
  }

  public HashMap(int initialCapacity_)
  {
    super(initialCapacity_);
  }

  public HashMap (Map<? extends Key, ? extends Value> map_)
  {
    super(map_);
  }

  /**
   * @return a collection of keys that you can use that are not subject to a ConcurrentModificationException
   */
  public Collection GetSafeKeys()
  {
    return new HashSet(keySet());
  }
  
  /**
   * looks for the requested value. if the value doesn't exist, it creates a new value of type <Value> and adds it to the map using key.
   * Type <Value> must have a default constructor (e.g. if Value is Foo, then Foo must have a "public void Foo()" constructor)
   * @param key_ Key
   * @param valueClass_ Class  it sure would be nice if i could eliminate this parm and use "Class<Value>.newInstance()" instead.
   * @return Value
   */
  public synchronized <T> Value GetSafe(Key key_, Class<T> valueClass_)
  {
    Value value = get(key_);
    if (value == null)
    {
      try
      {
        T newInstance = valueClass_.newInstance();
        value = (Value)newInstance;
      }
      catch (IllegalAccessException ex)
      {
        throw new RuntimeException(ex);
      }
      catch (InstantiationException ex)
      {
        throw new RuntimeException(ex);
      }
      put(key_,value);
    }

    return value;
  }

  public boolean GetAllowNullKey()
  {
    return _allowNullKey;
  }

  public void SetAllowNullKey(boolean allowNullKey_)
  {
    _allowNullKey = allowNullKey_;
  }

  public boolean GetAllowNullValue()
  {
    return _allowNullValue;
  }

  public void SetAllowNullValue(boolean allowNullValue_)
  {
    _allowNullValue = allowNullValue_;
  }

  /**
   * This operation does a "reverse lookup"
   *
   * @param value_
   * @return the key corresponding to the value or null if the value was not found
   */
  public Key KeyFor(Value value_)
  {
    for (Key key: keySet())
    {
      Value thisValue = get(key);
      if (thisValue.equals(value_) == true)
      {
        return key;
      }
    }

    return null;
  }

  public void SetAllowNulls (
    boolean allowNullKey_,
    boolean allowNullValue_ )
  {
    SetAllowNullKey(allowNullKey_);
    SetAllowNullValue(allowNullValue_);
  }

  @Override
  public Value put (
    Key   key,
    Value value )
  {
    if ( (_allowNullKey == false) &&                  // fast fail
         (key == null) )
    {
      throw new RuntimeException("Key is null. Null keys are disallowed.");
    }

    if ( (_allowNullValue == false) &&                // fast fail
         (value == null) )
    {
      throw new RuntimeException("Value is null. Null values are disallowed.");
    }

    return super.put(key,value);
  }

  @Override
  public Value get(Object key)
  {
    if ( (_allowNullKey == false) &&                  // fast fail
         (key == null) )
    {
      throw new RuntimeException("Key is null. Null keys are disallowed.");
    }

    return super.get(key);
  }

  @Override
  public boolean containsKey(Object key)
  {
    if ( (_allowNullKey == false) &&                  // fast fail
         (key == null) )
    {
      throw new RuntimeException("Key is null. Null keys are disallowed.");
    }

    return super.containsKey(key);
  }

}
