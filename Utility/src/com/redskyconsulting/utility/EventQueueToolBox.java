package com.redskyconsulting.utility;

import java.awt.AWTEvent;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.lang.reflect.Method;
import java.security.AccessController;
import java.security.PrivilegedAction;


/**
 * <p>Title: _EventQueueToolBox</p>
 * <p>Description: _EventQueueToolBox</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: </p>
 * @author Floyd Shackelford
 */

public class EventQueueToolBox
{

  protected static final Method  __dispatchEvent;
  static
  {
    __dispatchEvent = ToolBox.GetDeclaredMethod(java.awt.EventQueue.class,"dispatchEvent",false,java.awt.AWTEvent.class);
  }
 

  protected EventQueueToolBox()
  {
    super();
  }


  /**
   * use this for long-running operations that run (or might run) on the event queue dispatch thread that want to give
   * the event queue some processing time during the long-running operation.
   *
   * if this is the event queue dispatch thread (i.e. EventQueue.isDispatchThread() == true):
   *   1. pumps events onto the EventQueue for the specified minimum milliseconds, then returns.
   *   2. if exitEarly == true, will also return as soon as the event queue is discovered to be empty.
   *
   * if this is not the event queue thread:
   *   if exitEarly == true: returns immediately with no events pumped
   *   if exitEarly == false: sleeps (i.e. suspends the thread) milliseconds before returning.
   */
  public synchronized static void PumpEvents (
    long    milliseconds,
    boolean exitEarly )
  {
    if (EventQueue.isDispatchThread() == true)
    {
      long startMilliseconds = System.currentTimeMillis();
      long durationMilliseconds = 0L;

      EventQueue eventQueue = GetEventQueue();
      do
      {
        durationMilliseconds = System.currentTimeMillis() - startMilliseconds;
        if (eventQueue.peekEvent() != null)
        {
          PumpEvent(eventQueue);   // pump event may block. we peeked first, but you never know ...
          Thread.yield();
        }
        else
        if (exitEarly == true)
        {
          Thread.yield();
          durationMilliseconds = milliseconds;
        }
      }
      while (durationMilliseconds < milliseconds);
    }
    else
    if (exitEarly == false)
    {
      try
      {
        Thread.sleep(milliseconds);
      }
      catch (InterruptedException ex)
      {
        // do nothing
      }
    }
    else
    {
      Thread.yield();
    }
  }

  public static EventQueue GetEventQueue()
  {
    return AccessController.doPrivileged
    (
      new PrivilegedAction<EventQueue>()
      {
        public EventQueue run()
        {
          return Toolkit.getDefaultToolkit().getSystemEventQueue();
        }
      }
    );
  }

  private static void PumpEvent(EventQueue queue)
  {
    try
    {
      AWTEvent event = queue.getNextEvent();  // may be blocking if no events
      __dispatchEvent.invoke(queue,event);
    }
    catch (InterruptedException ex)
    {
      // do nothing
    }
    catch (Throwable ex)
    {
      ex.printStackTrace();
    }
  }

}
