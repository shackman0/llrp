package com.redskyconsulting.utility;

import java.io.UnsupportedEncodingException;

/**
 * <p>Title: StringTools</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public class StringTools
{
  
  private StringTools()
  {
    super();
  }

  
  static final byte[] HEXES = "0123456789ABCDEF".getBytes();

  public static String GetHex(
    byte[] raw,
    int    offset,
    int    length )
  {
    if (raw == null)
    {
      return null;
    }
    
    byte[] hexChars = new byte[(length - offset) * 2];
    for (int index = offset; index < length; index++)
    {
      byte b = raw[index];
      int jndex = index * 2;
      hexChars[jndex] = HEXES[(b >> 4) & 0xF];
      hexChars[jndex+1] = HEXES[(b) & 0xF];
    }

    String hex = "";
    try
    {
      hex = new String(hexChars, "ASCII");
    }
    catch (UnsupportedEncodingException ex)
    {
      ex.printStackTrace();
    }
    
    return hex.toString();
  }
  
}
