package com.redskyconsulting.utility;

import java.util.Locale;




/**
 * <p>Title: AbstractRuntimeProperties</p>
 * <p>Description:AbstractRuntimeProperties </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Red Sky Consulting, LLC</p>
 * @author Floyd Shackelford
 */

public abstract class AbstractRuntimeProperties
  extends com.redskyconsulting.utility.Properties
{

  private static final long serialVersionUID = 1L;

  
  public enum Keys
  {
    LocaleLanguage;
  }
  
  
  /**
   * singleton
   */
  protected static AbstractRuntimeProperties  __runtimeProperties = null;
  /* child classes must provide these 2 methods:

  static 
  {
    __runtimeProperties = new FooRuntimeProperties();
    __runtimeProperties.load("foo.properties");
  }

  public static FooRuntimeProperties getInstance()
  {
    return (FooRuntimeProperties)__runtimeProperties;
  }
  */

  
  public static AbstractRuntimeProperties GetInstance()
  {
    return __runtimeProperties;
  }


  protected AbstractRuntimeProperties()
  {
    super();
  }

  public AbstractRuntimeProperties(String  filePathName_)
  {
    super(filePathName_);
  }

  public AbstractRuntimeProperties(
    String  filePathName_,
    String  zipFilePathName_ )
  {
    super(filePathName_,zipFilePathName_);
  }

  public AbstractRuntimeProperties(
    String  filePathName_,
    String  zipFilePathName_,
    boolean inJarFile_ )
  {
    super(filePathName_,zipFilePathName_,inJarFile_);
  }

  public AbstractRuntimeProperties(
    String  filePathName,
    boolean inJarFile )
  {
    super(filePathName,inJarFile);
  }

  @Override
  public void Load()
  {
    super.Load();
    
    // initialize defaults for missing values ...
    boolean save = false;

    if ( (get(Keys.LocaleLanguage.name()) == null) ||
         (get(Keys.LocaleLanguage.name()).length() == 0) ) 
    {
      SetLanguage(Constants.EN_US);
      save = true;
    }
    
    if (save == true)
    {
      Save();
    }
  }

  public void Save()
  {
    super.Save(GetHeaderString());
  }
  
  protected String GetHeaderString()
  {
    return " DO NOT MODIFY * DO NOT MODIFY * DO NOT MODIFY * DO NOT MODIFY * DO NOT MODIFY";
  }

  @Override
  public String remove(Object key_)
  {
    String previousValue = super.remove(key_);
    Save();

    return previousValue;
  }

  @Override
  public String put (
    String  key_,
    String  value_ )
  {
    String previousValue = super.put(key_,value_);
    Save();
    
    return previousValue;
  }
  
  public String GetLanguage()
  {
    return get(Keys.LocaleLanguage.name());
  }

  public void SetLanguage(String language_)
  {
    put(Keys.LocaleLanguage.name(),language_);
  }
  
  public Locale GetLocale()
  {
    String language = get(Keys.LocaleLanguage.name());
    if (language == null)
    {
      language = Constants.EN_US;
    }
    
    Locale locale = new Locale(language);
    
    return locale;
  }

  public void SetLocale(Locale locale_)
  {
    put(Keys.LocaleLanguage.name(),locale_.getLanguage());
  }
  
}
