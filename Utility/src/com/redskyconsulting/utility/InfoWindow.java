package com.redskyconsulting.utility;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;


/**
 * <p>Title: InfoWindow</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Red Sky Consulting, Ltd.</p>
 * @author Floyd Shackelford
 */
public class InfoWindow
  extends JFrame
{

  private static final long serialVersionUID = 1L;


  protected static final Dimension     PANEL_SIZE;

  protected static final InfoWindow    __infoWindow;
  static
  {
    PANEL_SIZE = new Dimension(600,300);
    __infoWindow = new InfoWindow().Initialize();
  }

  public static InfoWindow GetSingleton()
  {
    return __infoWindow;
  }

  protected static boolean             __copyErrors2SystemErr = true;
  protected static boolean             __copyInfo2SystemOut = false;




  protected Container                  _northPanel;
  protected Container                  _southPanel;
  protected Container                  _eastPanel;
  protected Container                  _westPanel;
  protected Container                  _centerPanel;

  protected JScrollPane                _scrollPane;
  protected JTextArea                  _textArea;



  protected static final String        NEW_LINE = "\n";

  protected static final String        TEXT_STYLE            = "TEXT_STYLE";
  protected static final String        EXCEPTION_STYLE       = "EXCEPTION_STYLE";



  private InfoWindow()
  {
    super();
  }

  public static boolean GetCopyErrors2SystemErr()
  {
    return __copyErrors2SystemErr;
  }

  public static void SetCopyErrors2SystemErr(boolean copyErrors2SystemErr)
  {
    __copyErrors2SystemErr = copyErrors2SystemErr;
  }

  public JTextArea GetTextArea()
  {
    return _textArea;
  }

  public JScrollPane GetScrollPane()
  {
    return _scrollPane;
  }

  public InfoWindow Initialize ()
  {
    addWindowListener(new WindowListener(this));

    setTitle("Information Window");
    setResizable(true);

    Container contentPane = getContentPane();

    // build north panel

    _northPanel = CreateNorthPanel();
    contentPane.add(_northPanel,BorderLayout.NORTH);

    // build center panel

    _centerPanel = CreateCenterPanel();
    contentPane.add(_centerPanel,BorderLayout.CENTER);

    // build south panel

    _southPanel = CreateSouthPanel();
    contentPane.add(_southPanel,BorderLayout.SOUTH);

    // build east panel

    _eastPanel = CreateEastPanel();
    contentPane.add(_eastPanel,BorderLayout.EAST);

    // build west panel

    _westPanel = CreateWestPanel();
    contentPane.add(_westPanel,BorderLayout.WEST);

    validate();
    pack();
    SizeInitialPanel();
    PositionInitialPanel();

    return this;
  }

  protected Container CreateNorthPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected Container CreateCenterPanel()
  {
    _textArea = new JTextArea();
    _textArea.setEnabled(true);
    _textArea.setEditable(true);

    // the scroll pane let's us display a really long document
    _scrollPane = new JScrollPane();
    _scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
    _scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    _scrollPane.setViewportView(_textArea);

    return _scrollPane;
  }

  protected Container CreateWestPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected Container CreateEastPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected Container CreateSouthPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected void SizeInitialPanel()
  {
    setSize(PANEL_SIZE);
  }

  protected void PositionInitialPanel()
  {
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Random ran = new Random();
    int x = (screenSize.width - getWidth())/2;
    if (x > 0)
    {
      x = x + (ran.nextInt(x) - (x/2));
    }
    else
    {
      x = 0;
    }
    int y = (screenSize.height - getHeight())/2;
    if (y > 0)
    {
      y = y + (ran.nextInt(y) - (y/2));
    }
    else
    {
      y = 0;
    }
    setLocation(x,y);
  }

  public static class WindowListener
    extends WindowAdapter
  {
    protected InfoWindow _infoWindow;

    public WindowListener(InfoWindow infoWindow)
    {
      super();

      _infoWindow = infoWindow;
    }

    @Override
    public void windowClosing (WindowEvent event)
    {
      _infoWindow.setVisible(false);
    }
  }

  protected static final boolean IS_HEADLESS = GraphicsEnvironment.isHeadless();

  protected static void WriteText (
    String      displayString,
    JScrollPane scrollPane,
    JTextArea   textArea,
    PrintStream copy2PrintStream )
  {
    if (IS_HEADLESS == false)
    {
      scrollPane.getVerticalScrollBar().setValue(scrollPane.getVerticalScrollBar().getMaximum());   // scroll to the bottom
      int beginningLength = textArea.getDocument().getLength();   // save the current document length

      // Add DTS
      textArea.append(GregorianCalendar.GetDateNow() + " " + GregorianCalendar.GetTimeNow(true,true) + NEW_LINE);
      // Add Message
      textArea.append(displayString + NEW_LINE + NEW_LINE);
      // scroll to the top of what we just added
      textArea.setCaretPosition(beginningLength + 1);

      EventQueueToolBox.PumpEvents(100, true);
    }

    if ( ( (__copyInfo2SystemOut == true) ||
           (IS_HEADLESS == true) ) &&
         (copy2PrintStream == System.out) )
    {
      copy2PrintStream.println(displayString);
    }
    else
    if ( ( (__copyErrors2SystemErr == true) ||
           (IS_HEADLESS == true) ) &&
         (copy2PrintStream == System.err) )
    {
      copy2PrintStream.println(displayString);
    }
  }

  public static void Show()
  {
    if (__infoWindow.isVisible() == false)
    {
      __infoWindow.setVisible(true);
      __infoWindow.toFront();
    }
  }

  public static void DisplayText(
    String   displayString,
    boolean  forceShow )
  {
    WriteText(displayString,__infoWindow._scrollPane,__infoWindow._textArea, System.out);
    if (forceShow == true)
    {
      Show();
    }
  }

  public static void DisplayStackTrace(Throwable throwable)
  {
    DisplayStackTrace(throwable,true);
  }

  public static void DisplayStackTrace(
    Throwable throwable,
    boolean   forceShow )
  {
    StringWriter stringWriter = new StringWriter();
    PrintWriter printWriter = new PrintWriter(stringWriter);
    throwable.printStackTrace(printWriter);
    String displayString = stringWriter.toString();
    WriteText(displayString,__infoWindow._scrollPane,__infoWindow._textArea, System.err);
    if (forceShow == true)
    {
      Show();
    }
  }



}
