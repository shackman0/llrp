package com.redskyconsulting.utility;









/**
 * <p>Title: Globals</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Red Sky Consulting, LLC</p>
 * @author Floyd Shackelford
 */

public class Globals
{


//  public static Logger     __logger;
//  static
//  {
//    __logger = Logger.getLogger(Globals.class.getName());
//    __logger.setLevel(Level.ALL);
//    
//    __logger.setUseParentHandlers(false);
//    StreamHandler streamHandler = new StreamHandler(System.out, new SimpleFormatter());
//    __logger.addHandler(streamHandler);
//  }

  
  public enum RunMode
  {
    Production
    {
      @Override
      public boolean IsProduction()
      {
        return true;
      }
      
      @Override
      public boolean IsTest()
      {
        return false;
      }
    },
    
    Test
    {
      @Override
      public boolean IsProduction()
      {
        return false;
      }
      
      @Override
      public boolean IsTest()
      {
        return true;
      }
    };
    
    public abstract boolean IsProduction();
    public abstract boolean IsTest();
  }

  public static RunMode           runMode = RunMode.Production;
  
  
  public static final int         majorVersion           = 0;
  public static final int         minorVersion           = 0;
  public static final int         maintenanceVersion     = 0;

  
  
  /**
   * to define which log entries should be generated  
   *
   */
  public enum LogType
  {
    PRODUCTION,
    DEBUG;
  }
  
  /**
   * the iso langauge code to use when running the application. possibly may be overridden at startup.
   */
  public static String         language = "en_us";

  /**
   * the zip file containing the resources. null if resources are to be read directly from the resources directory.
   */
  public static String         resourcesZipFileName = null;  // "resources.zip";

  

  private Globals()
  {
    super();
  }

  
  /**
   * we use this for adding spaces before text
   */
  public static final String SPACES  = "                                                                                                                                                                                                                          ";

  public static String Spaces (int desiredColumn)
  {
    return Spaces(0,desiredColumn);
  }

  /**
   * creates a string spacer to align text
   *
   * @param prefixLength int the length of the text to the left of the text to be aligned
   * @param desiredColumn int the column in which you want the text aligned
   * @return String the right number of spaces to align your text at the column specified
   */
  public static String Spaces (
    int prefixLength,
    int desiredColumn )
  {
    return SPACES.substring(0,Math.max(0,desiredColumn - prefixLength));
  }
  
}
