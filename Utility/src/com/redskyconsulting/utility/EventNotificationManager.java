package com.redskyconsulting.utility;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;


/**
 * <p>Title: Event Notification Manager</p>
 * <p>Description: handles sending event notifications to registered listeners</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Red Sky Consulting, LLC</p>
 * @author Floyd Shackelford
 */

public class EventNotificationManager<EventType extends EventNotificationManagerEvent, ListenerType>
{

  /**
   * HashSet of EventListeners
   * This keeps track of the EventListeners that want to be notified whenever certain actions occur
   */
  protected com.redskyconsulting.utility.ArrayList<ListenerType>   _eventListeners = new com.redskyconsulting.utility.ArrayList();

  /**
   * if you want to turn off notifications, then set this to false
   */
  protected boolean               _notificationEnabled = true;


  public EventNotificationManager()
  {
    super();
  }

  public void AddListener(ListenerType listener_)
  {
    _eventListeners.add(listener_);
  }

  public void RemoveListener(ListenerType listener_)
  {
    _eventListeners.remove(listener_);
  }

  public void RemoveAllListeners()
  {
    _eventListeners = new com.redskyconsulting.utility.ArrayList();
  }

  public com.redskyconsulting.utility.ArrayList<ListenerType> GetListeners()
  {
    return _eventListeners;
  }

  public boolean IsNotificationEnabled()
  {
    return _notificationEnabled;
  }

  public void SetNotificationEnabled(boolean notificationEnabled_)
  {
    this._notificationEnabled = notificationEnabled_;
  }

  /**
   * this method routes the notification to the specified method
   * @param methodName_ the name of the event starting method to which to route the message
   * @param event_ the event to pass to the listener's method
   */
  public void NotifyListenersOfEvent (
    String     methodName_,
    EventType  event_ )
  {
    if (_notificationEnabled == true)
    {
      com.redskyconsulting.utility.ArrayList<ListenerType> tempEventListeners = new com.redskyconsulting.utility.ArrayList(_eventListeners);
      for (Iterator<ListenerType> iter = tempEventListeners.iterator(); ((event_.GetConsumed() == false) && (iter.hasNext())); )
      {
        ListenerType eventListener = (iter.next());
        Class eventClass = event_.getClass();
        Method method = null;
        try
        {
          method = eventListener.getClass().getMethod( methodName_, eventClass );
        }
        catch ( SecurityException ex )
        {
          throw new RuntimeException(ex);
        }
        catch ( NoSuchMethodException ex )
        {
          throw new RuntimeException(ex);
        }

        try
        {
          method.setAccessible(true);
          method.invoke(eventListener, event_);
        }
        catch (InvocationTargetException ex)
        {
          throw new RuntimeException(ex);
        }
        catch (IllegalAccessException ex)
        {
          throw new RuntimeException(ex);
        }
      }
    }
  }

}
