package com.redskyconsulting.utility;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;


/**
 * <p>Title: ResourceBundle</p>
 * <p>Description: ResourceBundle</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: </p>
 * @author Floyd Shackelford
 * @version $Header: p:/cvs repository/databright/Utility/src/com/redskyconsulting/utility/ResourceBundle.java,v 1.2 2010/07/19 22:07:39 noyb Exp $
 */

public class ResourceBundle
{

  protected static final String   RESOURCES_TOP_DIRECTORY       = "./resources";
  protected static final String   RESOURCES_FILE_NAME_EXTENSION = ".resources";

  public static final String      TIP_SUFFIX     = "_TIP";
  public static final String      TITLE_SUFFIX   = "_TITLE";
  public static final String      MSG_SUFFIX     = "_MSG";

  protected static final String INFO_KEY_PREFIX       = "INFO_";
  protected static final String WARNING_KEY_PREFIX    = "WARNING_";
  protected static final String ERROR_KEY_PREFIX      = "ERROR_";
  protected static final String EXCEPTION_KEY_PREFIX  = "EXCEPTION_";


  public enum Search
  {
    KeyContains
    {
      @Override
      public boolean Accept(
        String key,
        String keyFragment)
      {
        return (key.indexOf(keyFragment) >= 0);
      }
    },

    KeyStartsWith
    {
      @Override
      public boolean Accept(
        String key,
        String keyFragment)
      {
        return (key.startsWith(keyFragment) == true);
      }
    },

    KeyEndsWith
    {
      @Override
      public boolean Accept(
        String key,
        String keyFragment)
      {
        return (key.endsWith(keyFragment) == true);
      }
    },

    AnyKey
    {
      @Override
      public boolean Accept(
        String key,
        String keyFragment)
      {
        return true;
      }
    };

    public abstract boolean Accept(
      String key,
      String keyFragment);
  }


  /**
   * we use this so that we only load exactly one copy of any particular resources into memory.
   *
   * key = @See GenerateResourceBundleKey()
   * value = ResourceBundle
   */
  protected static com.redskyconsulting.utility.HashMap<String,com.redskyconsulting.utility.ResourceBundle>   __resourceBundles = new com.redskyconsulting.utility.HashMap();

  /**
   * Name of the resourceBundle:  PackageName.ClassName (e.g. aPackage.Foo)
   */
  protected String                _resourceBundleName = null;

  protected String                _language = null;

  /**
   * key = Resource Key (String)
   * value = Resource (String)
   */
  protected com.redskyconsulting.utility.Properties           _resources = null;



  /**
   *
   * @return com.redskyconsulting.utility.HashMap
   */
  public static com.redskyconsulting.utility.HashMap<String,com.redskyconsulting.utility.ResourceBundle> GetAllResourceBundles()
  {
    return __resourceBundles;
  }

  /**
   *
   * @param language_ String
   * @param resourceOwnerClass Class
   * @return String
   */
  protected static String GenerateResourceBundleKey (
    String  bundleName_,
    String  language_ )
  {
    String resourceBundleKey = bundleName_ + "," + language_;
    return resourceBundleKey;
  }

  /**
   *
   * @param resourceBundle_ ResourceBundle
   */
  protected static void PutResourceBundleIntoCache(ResourceBundle resourceBundle_)
  {
    String resourceBundleKey = resourceBundle_.GenerateResourceBundleKey();
    if (__resourceBundles.put(resourceBundleKey, resourceBundle_) != null)
    {
      throw new RuntimeException("Resource bundle having key \"" + resourceBundleKey + "\" is already in the cache");
    }
  }

  /**
   *
   * @param ownerClass Class
   * @param language_ String
   * @return ResourceBundle the specified resource bundle from the resource bundles cache. null if not in cache.
   */
  public static ResourceBundle GetResourceBundleFromCache (
    String bundleName_,
    String language_ )
  {
    String myResourceBundleKey = GenerateResourceBundleKey(bundleName_,language_);
    ResourceBundle resourceBundle = __resourceBundles.get(myResourceBundleKey);
    return resourceBundle;
  }

  public static ResourceBundle GetResourceBundleFromCache (String bundleName)
  {
    return GetResourceBundleFromCache(bundleName, AbstractRuntimeProperties.GetInstance().GetLanguage());
  }

  /**
   *
   * @param bundleName_ ResourceBundleName
   * @param language_ String
   * @return ResourceBundle
   */
  public static synchronized ResourceBundle GetResourceBundle (
    String  bundleName_,
    String  language_ )
  {
    ResourceBundle resourceBundle = GetResourceBundleFromCache(bundleName_,language_);
    if (resourceBundle == null)
    {
      resourceBundle = new ResourceBundle(bundleName_,language_);
    }
    return resourceBundle;
  }

  public static ResourceBundle GetResourceBundle (String bundleName_)
  {
    return GetResourceBundle(bundleName_, AbstractRuntimeProperties.GetInstance().GetLanguage());
  }

  /**
   *
   * @param resourceOwnerClass Class
   * @param language String
   * @return ResourceBundle
   */
  public static synchronized ResourceBundle GetResourceBundle (
    Class  resourceOwnerClass,
    String language )
  {
    return GetResourceBundle(resourceOwnerClass.getName(), language);
  }

  public static ResourceBundle GetResourceBundle (Class resourceOwnerClass)
  {
    return GetResourceBundle(resourceOwnerClass, AbstractRuntimeProperties.GetInstance().GetLanguage());
  }

  public static ResourceBundle GetSafeResourceBundle (Class resourceOwnerClass)
  {
    ResourceBundle resourceBundle = GetResourceBundle(resourceOwnerClass, AbstractRuntimeProperties.GetInstance().GetLanguage());
    if (resourceBundle == null) 
    {
      resourceBundle = GetResourceBundle(resourceOwnerClass, com.redskyconsulting.utility.Constants.EN_US);
    }

    return resourceBundle;
  }

  /**
   * does a "reverse lookup" of the resources looking for the value, returning the key
   * @param ownerClass Class
   * @param value String
   * @return String
   */
  public static String GetResourceKey (
    Class   ownerClass,
    String  language,
    String  value,
    boolean verbose )
  {
    ResourceBundle resourceBundle = GetResourceBundle(ownerClass,language);
    return resourceBundle.GetResourceKey(value,verbose);
  }

  public static String GetResourceKey (
    Class   ownerClass,
    String  value,
    boolean verbose )
  {
    return GetResourceKey(ownerClass,AbstractRuntimeProperties.GetInstance().GetLanguage(),value,verbose);
  }

  /**
   * does a "reverse lookup" of the resources looking for the value, returning the key
   * @param ownerClass Class
   * @param value String
   * @return String
   */
  public static String GetResourceKey (
    com.redskyconsulting.utility.ArrayList<Class>  ownerClasses,
    String             language,
    String             value,
    boolean            verbose )
  {
    for (Class ownerClass : ownerClasses)
    {
      String resourceKey = ResourceBundle.GetResourceKey(ownerClass,language,value,verbose);
      if (resourceKey != null)
      {
        return resourceKey;
      }
    }
    return null;
  }

  public static String GetResourceKey (
    com.redskyconsulting.utility.ArrayList<Class>  ownerClasses,
    String             value,
    boolean            verbose )
  {
    return GetResourceKey(ownerClasses,AbstractRuntimeProperties.GetInstance().GetLanguage(),value,verbose);
  }
  

  /**
   *
   * @param language String
   * @param object Object
   */
  public ResourceBundle (
    String  bundleName,
    String  language )
  {
    super();

    _resourceBundleName = bundleName;
    this._language = language;
    LoadResources();
  }

  public ResourceBundle (String bundleName)
  {
    super();

    _resourceBundleName = bundleName;
    _language = AbstractRuntimeProperties.GetInstance().GetLanguage();
    LoadResources();
  }

  /**
   *
   */
  protected void PutIntoCache()
  {
    PutResourceBundleIntoCache(this);
  }

  protected String GetResourceFilePath (
    String language,
    String name )
  {
    String relativePath = RESOURCES_TOP_DIRECTORY +
                          com.redskyconsulting.utility.Constants.FILE_PATH_SEPARATOR +
                          language +
                          com.redskyconsulting.utility.Constants.FILE_PATH_SEPARATOR +
                          name +
                          RESOURCES_FILE_NAME_EXTENSION;

    return relativePath;
  }

  protected String GetResourceFilePath (String name)
  {
    return GetResourceFilePath(AbstractRuntimeProperties.GetInstance().GetLanguage(),name);
  }

  protected ZipEntry GetResourceZipEntry(ZipFile zipFile)
  {
    return GetResourceZipEntry(zipFile,_language,_resourceBundleName);
  }

  protected ZipEntry GetResourceZipEntry (
    ZipFile zipFile,  
    String language,
    String name )
  {
    String relativePath = language +
                          com.redskyconsulting.utility.Constants.FILE_PATH_SEPARATOR +
                          name +
                          RESOURCES_FILE_NAME_EXTENSION;

    return zipFile.getEntry(relativePath);
  }

  protected ZipEntry GetResourceZipEntry(
    ZipFile zipFile,
    String  entryName)
  {
    return GetResourceZipEntry(zipFile,AbstractRuntimeProperties.GetInstance().GetLanguage(),entryName);
  }

  /**
   *
   */
  protected void LoadResources()
  {
    String resourceFilePath = GetResourceFilePath(_resourceBundleName);
    com.redskyconsulting.utility.Properties resources = null;
    if (Globals.resourcesZipFileName == null)
    {
      resources = new com.redskyconsulting.utility.Properties(resourceFilePath);
    }
    else
    {
      resources = new com.redskyconsulting.utility.Properties(resourceFilePath,Globals.resourcesZipFileName);
    }
    resources.Load();
    SetResources(resources);

    PutIntoCache();
  }

  /**
   *
   * @return String
   */
  public String GetLanguage()
  {
    return _language;
  }

  /**
   *
   * @param resources com.redskyconsulting.utility.Properties
   */
  protected void SetResources(com.redskyconsulting.utility.Properties resources)
  {
    this._resources = resources;
  }

  /**
   *
   * @return com.redskyconsulting.utility.Properties
   */
  public com.redskyconsulting.utility.Properties GetResources()
  {
    return _resources;
  }

  public String GetResource(String key)
  {
    return GetResource(key,true);
  }

  public String GetResource(
    String  key,
    boolean verbose )
  {
    String resource = GetResources().get(key);

    if ( (resource == null) &&
         (verbose == true) )
    {
      new RuntimeException(_resourceBundleName + ": value for key \"" + key + "\" not found.").printStackTrace();
    }

    return resource;
  }

  /**
   *
   * @param value String
   * @return String
   */
  public String GetResourceKey(
    String  value,
    boolean verbose )
  {
    String resourceKey = GetResources().KeyFor(value);

    if ( (resourceKey == null) &&
         (verbose == true) )
    {
      new RuntimeException(_resourceBundleName + ": key for value \"" + value + "\" not found.").printStackTrace();
    }

    return resourceKey;
  }

  public String[] GetAllResources (String keyPrefix)
  {
    com.redskyconsulting.utility.HashMap<String,String> resourcesMap = GetAllResources(ResourceBundle.Search.KeyStartsWith, keyPrefix);
    int numberOfEntries = resourcesMap.size();

    String[] keys = new String[numberOfEntries];
    resourcesMap.keySet().toArray(keys);
    Arrays.sort(keys,new com.redskyconsulting.utility.ConstantsKeyComparator(keyPrefix));

    String[] resources = new String[numberOfEntries];
    for (int indx = 0; indx < numberOfEntries; indx++)
    {
      resources[indx] = resourcesMap.get(keys[indx]);
    }

    return resources;
  }

  /**
   * returns all the resources having a match on the key fragment
   * @param ownerClass Class
   * @param keySearchForLocation int KEY_SEARCH_PREFIX, KEY_SEARCH_CONTAINS, or KEY_SEARCH_SUFFIX
   * @param _language String
   * @param searchFor String
   * @return com.redskyconsulting.utility.HashMap
   */
  public com.redskyconsulting.utility.HashMap<String,String> GetAllResources (
    Search    keySearchForLocation,
    String    keyFragment )
  {
    Collection resourceKeys = GetAllResourceKeys(keySearchForLocation,keyFragment);
    com.redskyconsulting.utility.HashMap<String,String> resourcesTable = new com.redskyconsulting.utility.HashMap(resourceKeys.size());
    for (Iterator iter = resourceKeys.iterator(); iter.hasNext();)
    {
      String key = (String)(iter.next());
      resourcesTable.put(key,GetResource(key,true));
    }
    return resourcesTable;
  }

  /**
   * returns all the resource keys having a match on the key fragment
   * @param ownerClass Class
   * @param _language String
   * @param keySearchForLocation int KEY_SEARCH_PREFIX, KEY_SEARCH_CONTAINS, or KEY_SEARCH_SUFFIX
   * @param keyFragment String
   * @return com.redskyconsulting.utility.ArrayList of String keys
   */
  public com.redskyconsulting.utility.ArrayList<String> GetAllResourceKeys (
    Search    keySearchForLocation,
    String    keyFragment )
  {
    com.redskyconsulting.utility.ArrayList<String> keyList = GetResources().GetAllOrderedKeys();

    com.redskyconsulting.utility.ArrayList<String> keys = null;
    if (keySearchForLocation == Search.AnyKey)
    {
      keys = keyList;
    }
    else
    {
      keys = new com.redskyconsulting.utility.ArrayList(keyList.size());
      for (String key: keyList)
      {
        if (keySearchForLocation.Accept(key,keyFragment) == true)
        {
          keys.add(key);
        }
      }
    }

    return keys;
  }


  public String GetResourceTip (String resourceKey)
  {
    return GetResourceTip(resourceKey,true);
  }

  public String GetResourceTip (
    String  resourceKey,
    boolean verbose )
  {
    return GetResource(resourceKey + TIP_SUFFIX, verbose);
  }

  public String GetResourceTitle (String resourceKey)
  {
    return GetResourceTitle(resourceKey,true);
  }

  public String GetResourceTitle (
    String  resourceKey,
    boolean verbose )
  {
    return GetResource(resourceKey + TITLE_SUFFIX, verbose);
  }

  public String GetResourceMessage (String resourceKey)
  {
    return GetResourceMessage(resourceKey,true);
  }

  public String GetResourceMessage (
    String  resourceKey,
    boolean verbose )
  {
    return GetResource(resourceKey + MSG_SUFFIX, verbose);
  }

  public String GetResourceMessage (
    String     resourceKey,
    Object...  substitutionValues )
  {
    return GetResourceMessage(resourceKey,true,substitutionValues);
  }

  public String GetResourceMessage(
    String     resourceKey,
    boolean    verbose,
    Object...  substitutionValues )
  {
    return GetResource(resourceKey + MSG_SUFFIX, verbose, substitutionValues);
  }

  public String GetResource (
    String     resourceKey,
    Object...  substitutionValues )
  {
    return GetResource(resourceKey,true,substitutionValues);
  }

  public String GetResource (
    String     resourceKey,
    boolean    verbose,
    Object...  substitutionValues )
  {
    String formattedString = GetResource(resourceKey,verbose);

    if (formattedString != null) 
    {
      // convert Throwables into their innermost message
      for (int indx = 0; indx < substitutionValues.length; indx++)
      {
        if (substitutionValues[indx] instanceof Throwable)
        {
          substitutionValues[indx] = com.redskyconsulting.utility.ToolBox.GetInnermostMessage((Throwable)substitutionValues[indx]);
        }
      }
      
      formattedString = MessageFormat.format(formattedString, substitutionValues).trim();
    }

    return formattedString;
  }

  /**
   *
   * @return String
   */
  protected String GenerateResourceBundleKey()
  {
    return GenerateResourceBundleKey(_resourceBundleName,_language);
  }


}
