package com.redskyconsulting.utility;

import java.util.Collection;

/**
 * <p>Title: LinkedBlockingQueue</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: Databright Management Systems.</p>
 * @author Floyd Shackelford
 */
public class LinkedBlockingQueue<E>
  extends java.util.concurrent.LinkedBlockingQueue<E>
{

  private static final long serialVersionUID = 1L;

  
  public LinkedBlockingQueue()
  {
    super();
  }
  
  public LinkedBlockingQueue(Collection<? extends E> collection)
  {
    super(collection);
  }
  
  public LinkedBlockingQueue(int capacity)
  {
    super(capacity);
  }
  
  
  
}
