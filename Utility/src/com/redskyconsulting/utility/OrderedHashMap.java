package com.redskyconsulting.utility;

import java.util.Map;


/**
 * <p>Title: OrderedHashMap</p>
 * <p>Description: OrderedHashMap</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Red Sky Consulting, LLC</p>
 * @author Floyd Shackelford
 */

public class OrderedHashMap<Key extends Comparable, Value extends Comparable>
  extends com.redskyconsulting.utility.HashMap<Key,Value>
{

  private static final long serialVersionUID = 1L;

  
  protected com.redskyconsulting.utility.ArrayList<Key>     _orderedKeys = new com.redskyconsulting.utility.ArrayList<Key>();



  public OrderedHashMap()
  {
    super();
  }

  public OrderedHashMap (
    int   initialCapacity_,
    float loadFactor_ )
  {
    super(initialCapacity_,loadFactor_);
  }

  public OrderedHashMap(int initialCapacity_)
  {
    super(initialCapacity_);
  }

  public OrderedHashMap (Map<? extends Key, ? extends Value> map_)
  {
    super(map_);
  }

  /**
   * returns the keys in the order they were added.
   * @return com.redskyconsulting.utility.ArrayList
   */
  public com.redskyconsulting.utility.ArrayList<Key> GetOrderedKeys()
  {
    return _orderedKeys;
  }

  @Override
  public Value remove(Object key_)
  {
    _orderedKeys.remove(key_);

    return super.remove(key_);
  }

  /**
   * repeating a key causes the key to be moved down to the final time it was added.
   * @param key_ Key
   * @param value_ Value
   * @return Value
   */
  @Override
  public Value put (
    Key   key_,
    Value value_ )
  {
    Value previousValue = super.put(key_,value_);

    _orderedKeys.remove(key_);
    _orderedKeys.add(key_);

    return previousValue;
  }

}
