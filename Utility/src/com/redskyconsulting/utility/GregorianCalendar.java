package com.redskyconsulting.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;


/**
 * <p>Title: GregorianCalendar</p>
 * <p>Description: subclass of GregorianCalendar to give it additional functionality.</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: </p>
 * @author Floyd Shackelford
 * @version $Header: p:/cvs repository/databright/Utility/src/com/redskyconsulting/utility/GregorianCalendar.java,v 1.1 2010/07/16 17:59:47 noyb Exp $
 */

public class GregorianCalendar
  extends java.util.GregorianCalendar
{

  private static final long serialVersionUID = 65784930292L;
  

  public static final long  MILLISECONDS_IN_A_SECOND   = 1000L;
  public static final long  MILLISECONDS_IN_A_MINUTE   = MILLISECONDS_IN_A_SECOND * 60L;
  public static final long  MILLISECONDS_IN_AN_HOUR    = MILLISECONDS_IN_A_MINUTE * 60L;
  public static final long  MILLISECONDS_IN_A_DAY      = MILLISECONDS_IN_AN_HOUR * 24L;

  public static final String  DEFAULT_DATE_STRING_FORMAT           = "yyyy-MMM-dd";
  public static final String  DEFAULT_TIME_STRING_FORMAT           = "HH:mm:ss";
  public static final String  DEFAULT_TIME_STAMP_STRING_FORMAT     = "yyyy-MMM-dd HH:mm:ss";

  public static final int     COMPARE_TO_BEFORE    = -1;
  public static final int     COMPARE_TO_AFTER     = 1;
  public static final int     COMPARE_TO_EQUAL     = 0;



  /**
   * if you specify a format, we'll try it before we try our format list.
   */
  protected String                _format = null;


  public static GregorianCalendar Now()
  {
    GregorianCalendar now = new GregorianCalendar();
    return now;
  }

  /**
  * Constructor.
  */
  public GregorianCalendar()
  {
    super();
    Initialize();
  }

  public GregorianCalendar(long timeInMilliseconds)
  {
    super();
    Initialize(timeInMilliseconds);
  }

  /**
   * @param calendar
   */
  public GregorianCalendar(GregorianCalendar calendar)
  {
    super();

    setTimeInMillis(calendar.getTimeInMillis());
    Initialize(calendar);
  }

  public GregorianCalendar(String calendarString)
  {
    super();
    
    Initialize();

    try
    {
      SetDateWithFormat(calendarString,DEFAULT_DATE_STRING_FORMAT);
      _format = DEFAULT_DATE_STRING_FORMAT;
    }
    catch (ParseException ex1)
    {
      try
      {
        SetDateWithFormat(calendarString,DEFAULT_TIME_STAMP_STRING_FORMAT);
        _format = DEFAULT_TIME_STAMP_STRING_FORMAT;
      }
      catch (ParseException ex2)
      {
        try
        {
          SetDateWithFormat(calendarString,DEFAULT_TIME_STRING_FORMAT);
          _format = DEFAULT_TIME_STRING_FORMAT;
        }
        catch (ParseException ex3)
        {
          throw new RuntimeException("Parse error: calendar format is not recognized \"" + calendarString + "\"");
        }
      }
    }
  }


  /**
   * constructor.
   * you MUST call initialize after this constructor.
   * @param initialTimestamp
   */
  public GregorianCalendar (
    String calendarString,
    String format )
    throws ParseException
  {
    super();

    Initialize();

    // blank string sets date to now.
    if (calendarString.trim().length()<=0)
    {
      return;
    }

    _format = format;
    SetDateWithFormat(calendarString,format);
  }

  /**
   * Initializer.
   */
  protected void Initialize()
  {
    setLenient(false);
  }

  /**
   * Initializer.
   * @param initialCalendar
   */
  protected void Initialize (Calendar initialCalendar)
  {
    Initialize();
    setTime(initialCalendar.getTime());
  }

  /**
   * Initializer.
   * @param initialDate
   * @param dateFormat
   */
  protected void Initialize (
    String initialDate,
    String dateFormat )
  {
    Initialize();
    _format = dateFormat;
    SetSafeDate(initialDate,dateFormat);
  }

  /**
   * Initializer.
   * @param initialDate
   */
  protected void Initialize (java.util.Date initialDate)
  {
    Initialize();
    SetDate(initialDate);
  }

  /**
   * Initializer.
   * @param initialTime
   */
  protected void Initialize (long timeInMilliseconds)
  {
    Initialize();
    setTimeInMillis(timeInMilliseconds);
  }

  /**
   * set the time to 00:00:00.0
   * @return this
   */
  public GregorianCalendar SetTimeToEarliestInDay()
  {
    set(HOUR_OF_DAY,0);
    set(MINUTE,0);
    set(SECOND,0);
    set(MILLISECOND,0);

    return this;
  }

  /**
   * set the time to 23:59:59.999
   * @return this
   */
  public GregorianCalendar SetTimeToLatestInDay()
  {
    set(HOUR_OF_DAY,23);
    set(MINUTE,59);
    set(SECOND,59);
    set(MILLISECOND,999);

    return this;
  }

  /**
   * Setter.
   * @param initialTime
   */
  public void SetTime(java.sql.Time initialTime)
  {
    GregorianCalendar calendar = new GregorianCalendar();
    calendar.setTime(initialTime);

    set(HOUR_OF_DAY,calendar.get(HOUR_OF_DAY));
    set(MINUTE,calendar.get(MINUTE));
    set(SECOND,calendar.get(SECOND));
    set(MILLISECOND,0);
    set(ZONE_OFFSET,calendar.get(ZONE_OFFSET));
  }


  /**
   *
   * @return String the time in DEFAULT_TIME_STRING_FORMAT
   */
  public String GetTimeString(
    boolean includeSeconds,
    boolean includeMilliseconds )
  {
    StringBuilder timeBuffer = new StringBuilder();

    int hour = get(HOUR_OF_DAY);
    if (hour < 10)
    {
      timeBuffer.append("0");
    }
    timeBuffer.append(hour);

    timeBuffer.append(":");

    int minute = get(MINUTE);
    if (minute < 10)
    {
      timeBuffer.append("0");
    }
    timeBuffer.append(minute);

    if (includeSeconds == true)
    {
      timeBuffer.append(":");
  
      int second = get(SECOND);
      if (second < 10)
      {
        timeBuffer.append("0");
      }
      timeBuffer.append(second);
    
      if (includeMilliseconds == true)
      {
        timeBuffer.append(".");
        int millisecond = get(MILLISECOND);
        if (millisecond < 10)
        {
          timeBuffer.append("00");
        }
        else
        if (millisecond < 100)
        {
          timeBuffer.append("0");
        }
        timeBuffer.append(millisecond);
      }
    }
    
    return timeBuffer.toString();
  }

  /**
   * Getter
   * @return Time as String
   */
  public String GetTimeStringWithUTCOffSet(
    boolean includeSeconds,
    boolean includeMilliseconds )
  {
    StringBuilder timeBuffer = new StringBuilder();
    timeBuffer.append(GetTimeString(includeSeconds,includeMilliseconds));

    // Include UTC offset
    timeBuffer.append(" ");
    timeBuffer.append(GetUTCOffSet());

    return timeBuffer.toString();
  }

  /**
   * Setter.
   * @param format
   */
  public void SetFormat(String format)
  {
    _format = format;
  }

  /**
   * Getter.
   * @return _format
   */
  public String GetFormat()
  {
    return _format;
  }

  /**
   * Comparable interface.
   * @param obect the object to which to compare <em>this</em>
   * @return
   */
  @Override
  public int compareTo(Calendar otherCalendar)
  {
    long diffMiliSecs = CompareTo(otherCalendar);
    if (diffMiliSecs == 0)
    {
      return COMPARE_TO_EQUAL;
    }
    else
    if (diffMiliSecs < 0)
    {
      return COMPARE_TO_BEFORE;
    }
    else
    {
      return COMPARE_TO_AFTER;
    }
  }

  /**
   * Glorified version of compareTo method.
   *   returns a long instead of an int.  This is because int cannot contain the long returned by getTimeInMillis().
   * @param comparableCalendar calendar to which to compare <em>this</em>
   * @return difference in milliseconds ;
   *         Zero means they are the same ;
   *         negative values means this object is before the compareTo calendar ;
   *         Positive values means this object is after the compareTo calendar or that compareTo is null
   */
  public long CompareTo(Calendar comparableCalendar)
  {
    if (comparableCalendar == null)
    {
      return getTimeInMillis();
    }

    return getTimeInMillis() - (comparableCalendar.getTimeInMillis());
  }

  /**
   *
   * @param date
   * @return
   */
  public boolean EarlierThan(GregorianCalendar date)
  {
    return DaysBefore(date) > 0;
  }

  public boolean OnOrBefore(GregorianCalendar date)
  {
    return DaysBefore(date) >= 0;
  }

  /**
   *
   * @param date
   * @return
   */
  public boolean LaterThan(GregorianCalendar date)
  {
    return DaysBefore(date) < 0;
  }

  public boolean OnOrAfter(GregorianCalendar date)
  {
    return DaysBefore(date) <= 0;
  }

  /**
   *
   * @param date
   * @return
   */
  public boolean IsSameDateAs(GregorianCalendar date)
  {
    return DaysBefore(date) == 0;
  }

  /**
   *
   * @param calendar
   * @return
   */
  public int DaysAfter(GregorianCalendar calendar)
  {
    GregorianCalendar thisCalendar = (GregorianCalendar)clone();

    thisCalendar.ZeroTime();

    GregorianCalendar otherCalendar = (GregorianCalendar)(calendar.clone());
    otherCalendar.ZeroTime();

    long millis = thisCalendar.getTime().getTime() - otherCalendar.getTime().getTime();
    int numberOfDays = (int)(millis / MILLISECONDS_IN_A_DAY);

    return numberOfDays;
  }

  public void ZeroTime()
  {
    set(HOUR_OF_DAY,0);
    set(MINUTE,0);
    set(SECOND,0);
    set(MILLISECOND,0);
    complete();
  }

  /**
   *
   * @param calendar
   * @return
   */
  public int DaysBefore(GregorianCalendar calendar)
  {
    GregorianCalendar thisCalendar = (GregorianCalendar)clone();

    thisCalendar.ZeroTime();

    GregorianCalendar otherCalendar = (GregorianCalendar)(calendar.clone());
    otherCalendar.ZeroTime();

    long millis = otherCalendar.getTime().getTime() - thisCalendar.getTime().getTime();
    int numberOfDays = (int)(millis / MILLISECONDS_IN_A_DAY);

    return numberOfDays;
  }

  /**
   * Getter
   * @return
   */
  public String GetDateNumericString()
  {
    String dateString =
      String.valueOf(get(YEAR)) + "-" +
      String.valueOf(get(MONTH)+1) + "-" +
      String.valueOf(get(DAY_OF_MONTH));

    return dateString;
  }

  /**
   * Getter
   * @return the date in DEFAULT_DATE_STRING_FORMAT
   */
  public String GetDateString()
  {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(String.valueOf(get(YEAR)));
    stringBuilder.append("-");
    stringBuilder.append(Constants.GetShortMonths()[get(MONTH)]);
    stringBuilder.append("-");

    int dayOfMonth = get(DAY_OF_MONTH);
    if (dayOfMonth < 10)
    {
      stringBuilder.append("0");
    }
    stringBuilder.append(String.valueOf(dayOfMonth));

    return stringBuilder.toString();
  }

  /**
   * Setter.
   * @param date
   */
  public void SetDate(java.sql.Date date)
  {
    setTime(date);
  }

  /**
   * Setter.
   * @param date
   */
  public void SetDate(java.util.Date date)
  {
    setTime(date);
  }

  /**
   * setter.
   * @param date
   * @param dateFormat
   */
  public void SetSafeDate(String date, String dateFormat)
  {
    try
    {
      SetDateWithFormat(date,dateFormat);
    }
    catch (Exception ex)
    {
      // do nothing
    }
  }

  public static String GetNowAsTimestamp(
    boolean includeSeconds,
    boolean includeMilliseconds )
  {
    return new GregorianCalendar().GetAsTimestamp(includeSeconds,includeMilliseconds);
  }

  public static String GetDateNow()
  {
    return new GregorianCalendar().GetDateString();
  }

  public static String GetTimeNow(
    boolean includeSeconds,
    boolean includeMilliseconds )
  {
    return new GregorianCalendar().GetTimeString(includeSeconds,includeMilliseconds);
  }
  
  public long GetMillisecondsSinceMidnight()
  {
    int hours = get(HOUR_OF_DAY);
    int minutes = get(MINUTE);
    int seconds = get(SECOND);
    int milliseconds = get(MILLISECOND);
    
    long millisecondsSinceMidnight = (hours * MILLISECONDS_IN_AN_HOUR) + (minutes * MILLISECONDS_IN_A_MINUTE) + (seconds * MILLISECONDS_IN_A_SECOND) + milliseconds;
    
    return millisecondsSinceMidnight;
  }

  /**
   * Getter.
   * @return the DTS in DEFAULT_TIME_STAMP_STRING_FORMAT
   */
  public String GetAsTimestamp(
    boolean includeSeconds,
    boolean includeMilliseconds )
  {
    StringBuilder timeStamp = new StringBuilder();

    // append the date
    timeStamp.append(GetDateString());

    timeStamp.append(" ");

    // append the time
    timeStamp.append(GetTimeString(includeSeconds,includeMilliseconds));

    return timeStamp.toString();
  }

  /**
   * Getter
   * @return calendar's utc offset
   */
  public String GetUTCOffSet()
  {
    StringBuilder timeZoneOffset = new StringBuilder();

    TimeZone timeZone = getTimeZone();
    int gmtOffsetMillis = timeZone.getOffset(getTimeInMillis());
    int gmtOffsetHours = gmtOffsetMillis / (60*60*1000);    // convert milliseconds to whole hours
    int gmtOffsetMinutes = ((Math.abs(gmtOffsetMillis) - (Math.abs(gmtOffsetHours) * (60*60*1000))) / (60*1000));  // convert the fractional part into minutes

    if (gmtOffsetHours >= 0)
    {
      timeZoneOffset.append("+");
    }
    else
    {
      timeZoneOffset.append("-");
    }

    String offsetHours = String.valueOf(Math.abs(gmtOffsetHours));
    if (offsetHours.length() == 1)
    {
      offsetHours = "0" + offsetHours;
    }
    timeZoneOffset.append(offsetHours);

    String offsetMinutes = String.valueOf(gmtOffsetMinutes);
    if (offsetMinutes.length() == 1)
    {
      offsetMinutes = "0" + offsetMinutes;
    }
    timeZoneOffset.append(offsetMinutes);

    return timeZoneOffset.toString();
  }

  /**
   *
   * @return
   */
  @Override
  public String toString()
  {
    return GetAsTimestamp(true,false);
  }

  /**
   *
   * @param calendar
   * @return
   */
  public boolean Equals(GregorianCalendar calendar)
  {
    if (getTime().getTime() == calendar.getTime().getTime())
    {
      return true;
    }
    return false;
  }

  /**
   *
   * @param date
   * @param dateFormatString
   * @throws ParseException
   */
  public void SetDateWithFormat(String date,String dateFormatString)
    throws ParseException
  {
    java.util.Date newDate;

    if ( (date == null) ||
         (date.equals("") == true) )
    {
      newDate = getTime();
    }
    else
    {
      GregorianCalendar newCalendar = null;

      // If a Month is not specified, use current month
      if (dateFormatString.indexOf("M") < 0)
      {
        newCalendar = new GregorianCalendar();
        newCalendar.Initialize();

        dateFormatString = dateFormatString + "-MM";
        date = date + "-" + (newCalendar.get(Calendar.MONTH)+1);
      }
      // If a Year is not specified, use current year
      if (dateFormatString.indexOf("yy") < 0)
      {
        if (newCalendar == null)
        {
          newCalendar = new GregorianCalendar();
          newCalendar.Initialize();
        }
        dateFormatString = dateFormatString + "-yyyy";
        date = date + "-" + newCalendar.get(Calendar.YEAR);
      }

      // Use the SimpleDateFormat to get a date object from a dateString and a formatString.
      SimpleDateFormat dateFormat = new SimpleDateFormat(dateFormatString);

      newDate = dateFormat.parse(date); // this can throw an exception
    }

    setTime(newDate);
  }

  /**
   * Sets the time only, does not set the timezone or the date
   * @param calendar GregorianCalendar
   */
  public void SetTimeOnly(GregorianCalendar gregorianCalendar)
  {
    set(HOUR_OF_DAY, gregorianCalendar.get(HOUR_OF_DAY));
    set(MINUTE,      gregorianCalendar.get(MINUTE));
    set(SECOND,      gregorianCalendar.get(SECOND));
    set(MILLISECOND, 0);
    complete();
  }

  public void SetDateOnly(GregorianCalendar date)
  {
    set(YEAR,         date.get(YEAR));
    set(MONTH,        date.get(MONTH));
    set(DAY_OF_MONTH, date.get(DAY_OF_MONTH));
    complete();
  }

  @Override
  public void complete()
  {
    super.complete();
  }
  
  /**
   * Method used to set the calendar object to the correct time and time zone.
   * This is seperate from above setTimeZone() because setTimeZone() changes the time when called.
   * This method will set the timezone according to the TimeZone timezone paramter.
   * It will also set the calendar's Hour, Minute, and Second according to the GregorianCalendar time parameter
   * Note:  call complete!
   * @param time Calendar containing the correct time
   * @param timeZone Timezone to set the Calendar object to.
   */
  public void SetTimeAndTimeZone(GregorianCalendar gregorianCalendar, TimeZone timeZone)
  {
    setTimeZone(timeZone);
    SetTimeOnly(gregorianCalendar);
    complete(); //need to make sure calendar is in a completed state before returning.
  }

  public void SetDTS(GregorianCalendar date, TimeZone timeZone)
  {
    setTimeZone(timeZone);
    SetTimeOnly(date);
    SetDateOnly(date);
    complete(); //need to make sure calendar is in a completed state before returning.
  }


  /**
   *
   * @param start1 GregorianCalendar
   * @param end1 GregorianCalendar
   * @param start2 GregorianCalendar
   * @param end2 GregorianCalendar
   * @return boolean true if start1 -> end1 overlaps start2 -> end2.  false if these date ranges do not overlap.
   */
  public static boolean Overlaps (
    GregorianCalendar start1,
    GregorianCalendar end1,
    GregorianCalendar start2,
    GregorianCalendar end2 )
  {
    boolean overlaps =
      ( ( ( (start1.OnOrBefore(start2)) &&
            (end1.OnOrAfter(start2)) ) ||
          ( (start1.OnOrBefore(end2)) &&
            (end1.OnOrAfter(end2)) ) ) ||
        ( ( (start2.OnOrBefore(start1)) &&
            (end2.OnOrAfter(start1)) ) ||
          ( (start2.OnOrBefore(end1)) &&
            (end2.OnOrAfter(end1)) ) ) );

    return overlaps;
  }


}
