package com.redskyconsulting.utility;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Collection;

/**
 * <p>Title: ArrayList</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Red Sky Consulting, LLC</p>
 * @author Floyd Shackelford
 */

@SuppressWarnings("serial")
public class ArrayList<ElementType>
  extends java.util.ArrayList<ElementType>
  implements Serializable
{

  protected Class<?>              _elementType = null;
  

  public ArrayList()
  {
    super();
  }

  public ArrayList(Class<?> elementType_)
  {
    super();

    _elementType = elementType_;
  }

  public ArrayList(
    int      size_,
    Class<?> elementType_ )
  {
    super(size_);

    _elementType = elementType_;
  }

  public ArrayList(int size)
  {
    super(size);
  }

  public ArrayList(Collection<? extends ElementType> collection)
  {
    super(collection);
  }

  public ArrayList(
    Collection<? extends ElementType> collection,
    Class<?>                          elementType )
  {
    super(collection);
    
    _elementType = elementType;
  }


  public ArrayList(ElementType[] array)
  {
    this(array.length,array.getClass().getComponentType());

    for (ElementType element: array)
    {
      add(element);
    }
  }

  public void SetElementType(Class<?> elementType_)
  {
    _elementType = elementType_;
  }

  public Class<?> GetElementType()
  {
    return _elementType;
  }

  public void AddAllUnique(Collection<ElementType> collection)
  {
    for (ElementType element: collection)
    {
      AddUnique(element);
    }
  }
  
  /**
   * clears the array list and initializes it with size fillElements
   */
  public void Fill (
    ElementType fillElement,
    int         size )
  {
    clear();
    for (int indx = 0; indx < size; indx++)
    {
      add(fillElement);
    }
  }

  /**
   * add the item to the end of the list if it already doesn't exist in the list.
   *
   * @param element_ ElementType
   * @return boolean true = added; false = already present, not added
   */
  public boolean AddUnique(ElementType element_)
  {
    boolean unique = (contains(element_) == false);

    if (unique == true)
    {
      add(element_);
    }

    return unique;
  }

  public void AddAll(ElementType[] array_)
  {
    ensureCapacity(array_.length);
    for (int indx = 0; indx < array_.length; indx++)
    {
      add(array_[indx]);
    }
  }

  /**
   * creates an array
   * @return ElementType[]
   */
  public ElementType[] ToArray(Class<?> elementType_)
  {
    ElementType[] array = (ElementType[])(Array.newInstance(elementType_,size()));
    return toArray(array);
  }

  /**
   * creates an array
   * @return Object[]
   */
  public ElementType[] ToArray()
  {
    ElementType[] array = (ElementType[])(Array.newInstance(_elementType,size()));
    return toArray(array);
  }

  /**
   * this only works if all the elements are Integers. if they aren't you'll get a class cast exception.
   * @return int[]
   */
  public int[] ToIntArray()
  {
    int size = size();
    int[] intArray = new int[size];

    for (int index = 0; index < size; index++)
    {
      Object element = get(index);
      Integer integer = (Integer)element;
      intArray[index] = integer.intValue();
    }

    return intArray;
  }

  /**
   * return the list of elements as a single string constructed as:
   *   element0.toString() + "\n" + element1.toStirng() + "\n" + ... + elementLast.toString()
   */
  public String ToLines()
  {
    StringBuilder stringBuilder = new StringBuilder();

    for (ElementType element: this)
    {
      stringBuilder.append(element.toString());
      stringBuilder.append("\n");
    }

    int length = stringBuilder.length();
    if (length > 0)
    {
      // remove the trailing "\n"
      stringBuilder.setLength(length - 1);
    }
    
    return stringBuilder.toString();
  }

  public Object[] GetElementData()
  {
    Field elementDataField;
    try
    {
      elementDataField = java.util.ArrayList.class.getDeclaredField("elementData");
    }
    catch (SecurityException ex)
    {
      throw ex;
    }
    catch (NoSuchFieldException ex)
    {
      throw new RuntimeException(ex);
    }

    ElementType[] elementData;
    try
    {
      elementDataField.setAccessible(true);
      elementData = (ElementType[])(elementDataField.get(this));
    }
    catch (IllegalArgumentException ex)
    {
      throw ex;
    }
    catch (IllegalAccessException ex)
    {
      throw new RuntimeException(ex);
    }
    
    return elementData;
  }
  
}


