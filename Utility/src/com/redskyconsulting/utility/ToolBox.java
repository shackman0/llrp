package com.redskyconsulting.utility;

import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.text.ParseException;

import javax.swing.JFrame;

/**
 * <p>
 * Title: ToolBox
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2010
 * </p>
 * <p>
 * Company: Databright Management Systems.
 * </p>
 * 
 * @author Floyd Shackelford
 */
public class ToolBox
{

  private ToolBox()
  {
    super();
  }

  public static boolean Pause(long millis_)
  {
    try
    {
      Thread.sleep(millis_);
    }
    catch (InterruptedException ex)
    {
      ex.printStackTrace();
    }

    return true;
  }

  public static String GetParentPathName(String filePathName_)
  {
    File file = new File(filePathName_);
    String parentPathName = file.getParent();
    if (parentPathName == null)
    {
      parentPathName = File.separator;
    }
    else
      if (parentPathName.endsWith(File.separator) == false)
      {
        parentPathName += File.separator;
      }

    return parentPathName;
  }

  public static String GetInnermostMessage(Throwable throwable)
  {
    String message = GetInnermostCause(throwable).getClass().getName() + ": " + throwable.getLocalizedMessage();

    return message;
  }

  public static Throwable GetInnermostCause(Throwable throwable)
  {
    while (throwable.getCause() != null)
    {
      throwable = throwable.getCause();
    }

    return throwable;
  }

  /**
   * 0 = dts 1 = class simple name 2 = method name 3 = file name 4 = line number 5 = class long name 6 = message
   */
  protected static final String LOG_MESSAGE_FORMAT = "================\n{0} {1}.{2}() ({3}:{4}) {5}\n{6}\n================";

  public static void Log(String message)
  {
    Log(message, new Throwable().getStackTrace()[1]);
  }

  public static void Log(String message, StackTraceElement stackTraceElement)
  {
    InfoWindow.DisplayText(message, false);

    String dts = new com.redskyconsulting.utility.GregorianCalendar().GetAsTimestamp(true, true);
    String classLongName = stackTraceElement.getClassName();
    String classSimpleName = classLongName.substring(classLongName.lastIndexOf(".") + 1);
    String methodName = stackTraceElement.getMethodName();
    String fileName = stackTraceElement.getFileName();
    String lineNumber = String.valueOf(stackTraceElement.getLineNumber());

    String logMessage = MessageFormat.format(LOG_MESSAGE_FORMAT, dts, classSimpleName, methodName, fileName, lineNumber, classLongName, message);

    System.out.println(logMessage);
  }

  public static void Log(Exception ex)
  {
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    PrintStream printStream = new PrintStream(byteArrayOutputStream);
    ex.printStackTrace(printStream);
    String stackTrace = new String(byteArrayOutputStream.toByteArray());
    Log(stackTrace, ex.getStackTrace()[1]);
  }

  /**
   * here are examples of UTC dts: without microseconds: "2010-07-20T08:42:00.739-05:00" with microseconds:
   * "2010-07-20T08:42:00.739000-05:00"
   * 
   * @param dtsUTC
   * @param includesMicroseconds
   * @return
   * @throws ParseException
   */
  public static GregorianCalendar ConvertDTSUTC2GregorianCalendar(String dtsUTC)
    throws ParseException
  {
    dtsUTC = dtsUTC.replaceFirst("T", " ");
    int tzColonIndex = dtsUTC.lastIndexOf(":");
    dtsUTC = dtsUTC.substring(0, tzColonIndex) + dtsUTC.substring(tzColonIndex + 1);

    return new GregorianCalendar(dtsUTC, "yyyy-MM-DD HH:mm:ss.SSSSSSZ");
  }

  // public static void main(String... args)
  // {
  // try
  // {
  // ConvertDTSUTC2GregorianCalendar("2010-07-21T10:02:38.702-05:00");
  // ConvertDTSUTC2GregorianCalendar("2010-07-21T10:02:38.702000-05:00");
  // }
  // catch (ParseException ex)
  // {
  // ex.printStackTrace();
  // }
  // }

  public static void SetLocation2CenterOfScreen(JFrame frame)
  {
    Toolkit kit = frame.getToolkit();
    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    GraphicsDevice[] gs = ge.getScreenDevices();
    Insets in = kit.getScreenInsets(gs[0].getDefaultConfiguration());

    Dimension d = kit.getScreenSize();
    int max_width = (d.width - in.left - in.right);
    int max_height = (d.height - in.top - in.bottom);
    frame.setLocation((max_width - frame.getWidth()) / 2, (max_height - frame.getHeight()) / 2);
  }

  public static Method GetDeclaredMethod(Class theClass, String methodName, boolean quiet, Class... parameterTypes)
  {
    try
    {
      Method method = theClass.getDeclaredMethod(methodName, parameterTypes);
      method.setAccessible(true);
      return method;
    }
    catch (NoSuchMethodException ex)
    {
      if (quiet == false)
      {
        throw new RuntimeException(ex);
      }
      return null;
    }
  }

  public static String GetOutput(Process process)
    throws IOException
  {
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
    StringBuilder stringBuilder = new StringBuilder();
    String line;
    while ((line = bufferedReader.readLine()) != null)
    {
      stringBuilder.append(line).append("\n");
    }
    String output = stringBuilder.toString();

    return output;
  }

}
