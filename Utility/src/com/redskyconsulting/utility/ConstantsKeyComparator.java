package com.redskyconsulting.utility;


/**
 * <p>Title: ConstantsKeyComparator</p>
 * <p>Description: ConstantsKeyComparator</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Red Sky Consulting, LLC</p>
 * @author Floyd Shackelford
 */
public class ConstantsKeyComparator
  implements java.util.Comparator
{
  protected String   _keyPrefix = null;

  public ConstantsKeyComparator(String keyPrefix_)
  {
    super();
    
    _keyPrefix = keyPrefix_;
  }

  public int compare(Object o1, Object o2)
  {
    String string1 = o1.toString().substring(_keyPrefix.length());
    Integer int1 = new Integer(string1);

    String string2 = o2.toString().substring(_keyPrefix.length());
    Integer int2 = new Integer(string2);

    return int1.compareTo(int2);
  }

  @Override
  public boolean equals(Object obj)
  {
    return false;
  }

  @Override
  public int hashCode()
  {
    return super.hashCode();
  }



}
