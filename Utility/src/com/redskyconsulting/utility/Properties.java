package com.redskyconsulting.utility;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * <p>Title: Properties</p>
 * <p>Description: Properties</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Red Sky Consulting, LLC</p>
 * @author Floyd Shackelford
 */

@SuppressWarnings("serial")
public class Properties
  extends OrderedHashMap<String,String>
{

  protected final static String   IMPORT    = "<IMPORT>";

  protected com.redskyconsulting.utility.Properties    _defaults = null;

  
  
  /**
   * the path name of the properties file to be read
   */
  protected String      _filePathName = null;
  
  /**
   * non-null if the properties file is in a zip file.
   */
  protected String      _zipFilePathName = null;
  
  /**
   * true means the properties file or zip file is in the application .jar (or .ear) file.
   */
  protected boolean     _inJarFile = false;
  
  /**
   * examples:
   * 
   * example 1:
   * 
   * filePathName = "foo.properties"
   * zipFilePathName = null
   * useJarFile = false;
   *   
   *   foo.properties is in the current directory on the native file system (aka local hard drive)
   * 
   * example 2:
   * 
   * filePathName = "foo.properties"
   * zipFilePathName = "bar.zip"
   * useJarFile = false;
   *  
   *   foo.properties is in the root directory of the bar.zip file. 
   *   bar.zip is in the current directory on the native file system.
   * 
   * example 3:
   * 
   * filePathName = "foo.properties"
   * zipFilePathName = "bar.zip"
   * useJarFile = false;
   * 
   * example 4:
   * 
   * filePathName = "foo.properties"
   * zipFilePathName = "bar.zip"
   * useJarFile = true;
   *   
   *   foo.properties is in the root directory of the bar.zip file. 
   *   bar.zip is in the application .jar file.
   * 
   * example 5:
   * 
   * filePathName = "foo.properties"
   * zipFilePathName = null
   * useJarFile = true;
   *   
   *   foo.properties is in the root directory of the application .jar file.
   *   
   */
  

  public Properties()
  {
    super();
  }

  public Properties(String  filePathName_)
  {
    super();
    
    _filePathName = filePathName_;
  }

  public Properties(
    String  filePathName_,
    String  zipFilePathName_ )
  {
    super();
    
    _filePathName = filePathName_;
    _zipFilePathName = zipFilePathName_;
  }

  public Properties(
    String  filePathName_,
    String  zipFilePathName_,
    boolean inJarFile_ )
  {
    super();
    
    _filePathName = filePathName_;
    _zipFilePathName = zipFilePathName_;
    _inJarFile = inJarFile_;
  }

  public Properties(
    String  filePathName,
    boolean inJarFile )
  {
    super();
    
    _filePathName = filePathName;
    _inJarFile = inJarFile;
  }

  public String GetFilePathName()
  {
    return _filePathName;
  }

  public String GetZipFilePathName()
  {
    return _zipFilePathName;
  }

  public boolean GetUseJarFile()
  {
    return _inJarFile;
  }
  
  public String Get(String key_)
  {
    String value = super.get(key_);
    if ( (value == null) &&
         (_defaults != null) )
    {
      value = _defaults.get(key_);
    }

    return value;
  }

  public Properties GetDefaults()
  {
    return _defaults;
  }

  public void SetDefaults(Properties defaults)
  {
    _defaults = defaults;
  }

  /**
   * follows the defaults chain to the "end of the line" and returns the final defaults properties
   * @return TMProperties
   */
  public com.redskyconsulting.utility.Properties GetDefaultsTail()
  {
    com.redskyconsulting.utility.Properties tail = this;
    while (tail.GetDefaults() != null)
    {
      tail = tail.GetDefaults();
    }

    return tail;
  }

  public void SetDefaultsAtEnd(com.redskyconsulting.utility.Properties defaults_)
  {
    GetDefaultsTail().SetDefaults(defaults_);
  }

  public void Load()
  {
    if (_zipFilePathName == null)
    {
      LoadFile(_filePathName);
    }
    else
    {
      LoadFileFromZip(_filePathName);
    }
  }

  protected void LoadFile(String filePathName_)
  {
    try
    {
      InputStream inputStream;
      if (_inJarFile == true)
      {
        inputStream = Properties.class.getResourceAsStream("/" + filePathName_);
        if (inputStream == null)
        {
          throw new RuntimeException(filePathName_ + " not found in .jar, .war, or .ear file");
        }
      }
      else
      {
        File file = new File(filePathName_);
        FileInputStream fileInputStream = new FileInputStream(file);
        inputStream = new BufferedInputStream(fileInputStream);
      }
      
      String path2TopDirectory = ToolBox.GetParentPathName(filePathName_);
      LoadFileHelper(path2TopDirectory,inputStream);
      inputStream.close();
    }
    catch (FileNotFoundException ex)
    {
      throw new RuntimeException(ex);
    }
    catch (IOException ex)
    {
      throw new RuntimeException(ex);
    }
  }

  private void LoadFileHelper (
    String      path2TopDirectory_,
    InputStream inputStream_ )
    throws IOException
  {
    OrderedProperties orderedProperties = new OrderedProperties();
    orderedProperties.load(inputStream_);
    for (String key: orderedProperties.getOrderedKeys())
    {
      String value = orderedProperties.getProperty(key);
      if (key.equalsIgnoreCase(IMPORT) == true)
      {
        String absolutePath = path2TopDirectory_ + value;
        LoadFile(absolutePath);
      }
      else
      {
        put(key, value);
      }
    }
  }

  protected void LoadFileFromZip(String filePathName_)
  {
    ZipInputStream zipInputStream;
    if (_inJarFile == true)
    {
      InputStream inputStream = getClass().getResourceAsStream(_zipFilePathName);
      zipInputStream = new ZipInputStream(inputStream);
    }
    else
    {
      File file = new File(_zipFilePathName);
      InputStream inputStream;
      try
      {
        inputStream = new FileInputStream(file);
      }
      catch (FileNotFoundException ex)
      {
        throw new RuntimeException(ex);
      }
      BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
      zipInputStream = new ZipInputStream(bufferedInputStream);
    }

    ZipEntry zipEntry;
    try
    {
      while ((zipEntry = zipInputStream.getNextEntry()) != null)
      {
        if (zipEntry.getName().equals(filePathName_))
        {
          break;
        }
      }
    }
    catch (IOException ex)
    {
      throw new RuntimeException(ex);
    }

    if (zipEntry == null)
    {
      throw new RuntimeException("cannot find zip entry " + filePathName_ + " in zip file " + _zipFilePathName);
    }
    
    String path2TopDirectory = ToolBox.GetParentPathName(filePathName_);
    LoadFileFromZipHelper(path2TopDirectory,zipInputStream);
    try
    {
      zipInputStream.close();
    }
    catch (IOException ex)
    {
      throw new RuntimeException(ex);
    }
  }

  protected void LoadFileFromZipHelper(
    String         path2TopDirectory_,
    ZipInputStream zipInputStream_ )
  {
    try
    {
      OrderedProperties orderedProperties = new OrderedProperties();
      orderedProperties.load(zipInputStream_);
      for (String key: orderedProperties.getOrderedKeys())
      {
        String value = orderedProperties.getProperty(key);
        if (key.equalsIgnoreCase(IMPORT) == true)
        {
          String importFilePathName = path2TopDirectory_ + value;
          LoadFileFromZip(importFilePathName);
        }
        else
        {
          put(key, value);
        }
      }
    }
    catch (FileNotFoundException ex)
    {
      throw new RuntimeException(ex);
    }
    catch (IOException ex)
    {
      throw new RuntimeException(ex);
    }
  }

  public void Save(String comment_)
  {
    if (_inJarFile == true)
    {
      throw new RuntimeException("cannot save to .jar file");
    }
    
    if (_zipFilePathName != null)
    {
      throw new RuntimeException("cannot save to .zip file");
    }
    
    Save(_filePathName,comment_);
  }
  
  protected void Save (
    String  propertiesFileName_,
    String  comment_ )
  {
    Save(new File(propertiesFileName_), comment_);
  }

  protected void Save(
    File   file_,
    String comment_ )
  {
    try
    {
      FileOutputStream fileOutputStream = new FileOutputStream(file_);
      BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
      PrintStream printStream = new PrintStream(bufferedOutputStream);
      Save(printStream,comment_);
      printStream.flush();
      printStream.close();
    }
    catch (IOException ex)
    {
      throw new RuntimeException(ex);
    }
  }

  /**
   * @todo IMPORTed properties should not be stored
   */
  protected void Save (
    PrintStream printStream_,
    String      comment_ )
    throws IOException
  {
    java.util.Properties properties = new java.util.Properties();
    for (String key: GetOrderedKeys())
    {
      String value = get(key);
      properties.put(key,value);
    }
    properties.store(printStream_,comment_);
  }

  /**
   * Combines the local .keySet() with the defaults key sets.
   * @return Set
   */
  public com.redskyconsulting.utility.ArrayList<String> GetAllOrderedKeys()
  {
    com.redskyconsulting.utility.ArrayList<String> allKeys = new com.redskyconsulting.utility.ArrayList();
    Properties properties = this;
    while (properties != null)
    {
      allKeys.AddAllUnique(properties.GetOrderedKeys());
      properties = properties.GetDefaults();
    }
    return allKeys;
  }

}


/**
 * used to keep track of the order of the properties in the properties file
 */
class OrderedProperties
  extends java.util.Properties
{
  private static final long serialVersionUID = 1L;
  
  protected com.redskyconsulting.utility.ArrayList<String>  _orderedKeys = new com.redskyconsulting.utility.ArrayList();

  @Override
  public synchronized Object put(Object key_, Object value_)
  {
    Object previousValue = super.put(key_,value_);
    _orderedKeys.AddUnique(key_.toString());

    return previousValue;
  }

  public com.redskyconsulting.utility.ArrayList<String> getOrderedKeys()
  {
    return _orderedKeys;
  }
}
