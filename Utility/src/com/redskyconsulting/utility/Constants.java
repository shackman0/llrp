package com.redskyconsulting.utility;

import java.math.BigDecimal;




/**
 * <p>Title: Constants</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Red Sky Consulting, LLC</p>
 * @author Floyd Shackelford
 */

public class Constants
{

  /**
   * constants
   */

  public static final int  KILOBYTES = 1024;
  public static final int  MEGABYTES = 1000 * KILOBYTES;
  public static final int  GIGABYTES = 1000 * MEGABYTES;
  
  
  /**
   * the language string consists of an ISO language code, followed by an optional locale code see http://www.iso.ch/
   */

  /**
   * the language string consists of an ISO language code, followed by an optional locale code
   * see http://www.iso.ch/
   */
  public static final String      EN_US = "en_us";      // us english
  public static final String      EN_CA = "en_ca";      // canadian english
  public static final String      EN_UK = "en_uk";      // uk english
  public static final String      ES_MX = "es_mx";      // mexican spanish
  public static final String      FR_CA = "fr_ca";      // canadian french
  public static final String      KO    = "ko";         // korean

  public static final String[]    ISO_LANGAUGE_CODES            = new String[] { EN_US, EN_CA, EN_UK, ES_MX, FR_CA, KO };

  // gui constants
  public static final int         INTER_FIELD_STRUT        = 5;
  public static final int         INTRA_FIELD_STRUT        = 3;
  public static final int         INTER_GROUP_STRUT        = INTER_FIELD_STRUT * 2;
  public static final int         INTER_FIELD_GAP          = 10;
  public static final int         INTRA_FIELD_GAP          = 5;

  public static final String      FILE_PATH_SEPARATOR                = "/";
  public static final char[]      FILE_NAME_REPLACEMENT_CHARACTERS   = { '.', ' ', '\\' };

  public static final Integer     INTEGER_ZERO                       = new Integer(0);
  public static final Integer     INTEGER_ONE                        = new Integer(1);
  public static final Integer     INTEGER_TEN                        = new Integer(10);
  public static final String      STRING_ONE                         = "1";
  public static final double      DOUBLE_ONE                         = 1.0d;
  public static final BigDecimal  BIG_DECIMAL_ZERO                   = BigDecimal.ZERO;
  public static final BigDecimal  BIG_DECIMAL_ONE                    = new BigDecimal(1);
  public static final BigDecimal  BIG_DECIMAL_ONE_HUNDRED            = new BigDecimal(100);

  public static final String      XML_FILE_NAME_EXTENSION            = ".xml";
  public static final String      BAK_DIRECTORY_EXTENSION            = "_bak";

  public static final String      UNDERSCORE                         = "_";

  public static final String      NEW_LINE                           = "\n";

  public static final String      DATABASE_PACKAGE                   = "database.";
  public static final String      UTILITY_PACKAGE                    = "utility.";
  public static final String      APPLICATION_PACKAGE                = "application.";

  public static final String      OBJREFS                            = "ObjRefs";
  public static final String      REFERENCE                          = "Reference";

  public static final String      ENTRY_PANEL                        = "EntryPanel";
  public static final String      LIST_PANEL                         = "ListPanel";

  /**
   * common entry field column sizes
   */
  public static final int         LEVEL_WIDTH              = 40;

  public static final int         NAME_COLUMNS             = 20;
  public static final int         DESCRIPTION_COLUMNS      = 60;
  public static final int         SEARCH_STRING_COLUMNS    = 40;
  public static final int         DATE_COLUMNS             = 10;
  public static final int         USER_DEFINED_COLUMNS     = 40;
  public static final int         COMMENTS_COLUMNS         = 45;
  public static final int         COMMENTS_ROWS            = 5;
  public static final int         ADDRESS_COLUMNS          = 45;
  public static final int         CITY_COLUMNS             = 20;
  public static final int         PROVINCE_COLUMNS         = 20;
  public static final int         POSTAL_CODE_COLUMNS      = 20;
  public static final int         PHONE_NUMBER_COLUMNS     = 20;
  public static final int         E_MAIL_ADDRESS_COLUMNS   = 30;

  /**
   * used to put specified resources into an array
   */
  public static final String      SHORT_MONTH_PREFIX                  = "SHORT_MONTH_";
  public static final String      LONG_MONTH_PREFIX                   = "LONG_MONTH_";
  public static final String      ONE_LETTER_DAY_PREFIX               = "ONE_LETTER_DAY_";
  public static final String      SHORT_DAY_PREFIX                    = "SHORT_DAY_";
  public static final String      LONG_DAY_PREFIX                     = "LONG_DAY_";
  public static final String      FONT_STYLES_PREFIX                  = "FONT_STYLES_";
  public static final String      COLORS_PREFIX                       = "COLORS_";
  
  public static final double      CENTIMETERS_PER_INCH     = 2.54d;

  /**
   * delay after which the progress monitor will appear
   */
  public static final int         PROGRESS_MONITOR_DELAY   = 2 * 1000;
    
  
  
  private Constants()
  {
    super();
  }
  
  public static String[] GetColors()
  {
    return GetResources(COLORS_PREFIX);
  }

  public static String[] GetFontStyles()
  {
    return GetResources(FONT_STYLES_PREFIX);
  }

  /**
   * Getter.
   * @return days of the week names
   */
  public static String[] GetDays()
  {
    return GetResources(LONG_DAY_PREFIX);
  }

  /**
   * Getter.
   * @return days of the week short names
   */
  public static String[] GetShortDays()
  {
    return GetResources(SHORT_DAY_PREFIX);
  }

  /**
   * Getter.
   * @return days of the week short names
   */
  public static String[] GetFirstLetterOfDays()
  {
    return GetResources(ONE_LETTER_DAY_PREFIX);
  }

  /**
   * Getter.
   * @return month names
   */
  public static String[] GetMonths()
  {
    return GetResources(LONG_MONTH_PREFIX);
  }

  /**
   * Getter.
   * @return short month names
   */
  public static String[] GetShortMonths()
  {
    return GetResources(SHORT_MONTH_PREFIX);
  }

  protected static com.redskyconsulting.utility.ResourceBundle resourceBundle = null;
  
  public static com.redskyconsulting.utility.ResourceBundle GetResourceBundle()
  {
    if (resourceBundle == null)
    {
      resourceBundle = com.redskyconsulting.utility.ResourceBundle.GetResourceBundle(Constants.class);
    }
    
    return resourceBundle;
  }
  
  public static String[] GetResources(String keyPrefix)
  {
    return GetResourceBundle().GetAllResources(keyPrefix);
  }

}
