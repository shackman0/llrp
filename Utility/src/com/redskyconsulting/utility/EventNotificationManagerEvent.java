package com.redskyconsulting.utility;

import java.util.GregorianCalendar;

/**
 * <p>Title: _EventNotificationManagerEvent</p>
 * <p>Description: this class is used to transport information to recipients of events</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Red Sky Consulting, LLC</p>
 * @author Floyd Shackelford
 */

public abstract class EventNotificationManagerEvent<Broadcaster>
{

  /**
   * the new edit button that is broadcasting this event
   */
  protected Broadcaster      _broadcaster = null;

  /**
   * indicates whether the event was successful (true) or not (false)
   */
  protected boolean          _success       = true;

  /**
   * keeps track of arbitrary message specified object
   */
  protected Object           _other = null;

  /**
   * if true, then the event was "consumed" by the most recently notified listener and notification of this event should cease immediately.
   */
  protected boolean          _consumed = false;

  /**
   * the DTS of when the event occurred.
   */
  protected com.redskyconsulting.utility.GregorianCalendar    _dts = new com.redskyconsulting.utility.GregorianCalendar();


  protected EventNotificationManagerEvent(
    Broadcaster   broadcaster_,
    boolean       success_ )
  {
    super();

    _broadcaster = broadcaster_;
    _success = success_;
  }

  protected EventNotificationManagerEvent(
    Broadcaster   broadcaster_,
    boolean       success_,
    Object        other_ )
  {
    this(broadcaster_,success_);

    _other = other_;
  }

  public Broadcaster GetBroadcaster()
  {
    return _broadcaster;
  }

  public Object GetOther()
  {
    return _other;
  }

  public GregorianCalendar GetDTS()
  {
    return _dts;
  }

  /**
   * Setter
   * @param success_ true if the event was successful; false otherwise. generally, start events always set this to true.
   */
  public void SetSuccess(boolean success_)
  {
    _success = success_;
  }

  public boolean WasSuccessful()
  {
    return _success;
  }

  public void SetConsumed(boolean consumed_)
  {
    _consumed = consumed_;
  }

  public boolean GetConsumed()
  {
    return _consumed;
  }


}
