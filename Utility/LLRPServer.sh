#! /bin/bash

# This script launches the LLRP Server application.
# be sure to review the USER MODIFIABLE SECTION


# clear all the variables we use in this script so we don't "inherit" any from the environment.
# do not modify this list.

JAVA2SE_BIN=
RUN_JAVA=
MINIMUM_MEMORY=
MAXIMUM_MEMORY=
CLASSPATH=
JVM_PARMS=


# ********** BEGIN USER MODIFIABLE SECTION **********
# ********** BEGIN USER MODIFIABLE SECTION **********
# ********** BEGIN USER MODIFIABLE SECTION **********


# ********************
# The JRE bin directory must be in your PATH environment variable or
# you must uncomment and the JAVA2SE_BIN variable to point to the top 
# of where the Java 2 SE runtime jar files are located. 
# Use an absolute path ending with a "/" (as seen here)

# JAVA2SE_BIN=/usr/local/jre6/bin/


# ********************
# Uncomment the RUN_JAVA you want to use to launch the application
# java will display a shell window.
# javaw hides the shell window. 
# this is a required variable

RUN_JAVA="${JAVA2SE_BIN}java"
# RUN_JAVA="%JAVA2SE_BIN%javaw"

# ********************
# memory size is in megabytes. 
# it is recommended that you NOT allocate less than MINIMUM_MEMORY=10 

MINIMUM_MEMORY=10
MAXIMUM_MEMORY=200


# ********** END USER MODIFIABLE SECTION **********
# ********** END USER MODIFIABLE SECTION **********
# ********** END USER MODIFIABLE SECTION **********



# ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********
# ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********
# ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********


# the JVM_PARMS to commands you want passed to the JVM
JVM_PARMS="-Xms${MINIMUM_MEMORY}m -Xmx${MAXIMUM_MEMORY}m"

# you shouldn't need to modify the classpath
CLASSPATH=".:./lib:./lib/llrpServer.jar:./lib/ltkjava-1.0.0.6-with-dependencies.jar:"


# launch the application
echo $RUN_JAVA $JVM_PARMS -cp $CLASSPATH com.databright.llrp.server.gui.ControlPanel
$RUN_JAVA $JVM_PARMS -cp $CLASSPATH com.databright.llrp.server.gui.ControlPanel





