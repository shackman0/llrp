@echo off

rem This script launches the LLRP Server application.
rem be sure to review the USER MODIFIABLE SECTION


rem clear all the variables we use in this script so we don't "inherit" any from the windows environment.
rem do not modify this list.

set JAVA2SE_BIN=
set RUN_JAVA=
set MINIMUM_MEMORY=
set MAXIMUM_MEMORY=
set CLASSPATH=
set JVM_PARMS=

rem ********** BEGIN USER MODIFIABLE SECTION **********
rem ********** BEGIN USER MODIFIABLE SECTION **********
rem ********** BEGIN USER MODIFIABLE SECTION **********


rem ********************
rem The JRE bin directory must be in your PATH environment variable or
rem you must uncomment and set the JAVA2SE_BIN variable to point to the top 
rem of where the Java 2 SE runtime jar files are located. 
rem Use an absolute path ending with a "\" (as seen here)

rem set JAVA2SE_BIN=C:\Program Files\Java\jre6\bin\


rem ********************
rem Uncomment the RUN_JAVA you want to use to launch the application
rem java will display a dos window.
rem javaw hides the dos window. 
rem this is a required variable

set RUN_JAVA=start "LLRP Server" /i /b "%JAVA2SE_BIN%java.exe"
rem set RUN_JAVA=start "LLRP Server" /i /b "%JAVA2SE_BIN%javaw.exe"


rem ********************
rem memory size is in megabytes. 
rem it is recommended that you NOT allocate less than MINIMUM_MEMORY=10 

set MINIMUM_MEMORY=10
set MAXIMUM_MEMORY=200


rem ********** END USER MODIFIABLE SECTION **********
rem ********** END USER MODIFIABLE SECTION **********
rem ********** END USER MODIFIABLE SECTION **********



rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********
rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********
rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********


rem set the JVM_PARMS to commands you want passed to the JVM
set JVM_PARMS=-Xms%MINIMUM_MEMORY%m -Xmx%MAXIMUM_MEMORY%m

rem you shouldn't need to modify the classpath
set CLASSPATH=".;./lib;./lib/llrpServer.jar;./lib/ltkjava-1.0.0.6-with-dependencies.jar;"

rem launch the application
%RUN_JAVA% %JVM_PARMS% -cp %CLASSPATH% com.databright.llrp.server.gui.ControlPanel


rem pause



