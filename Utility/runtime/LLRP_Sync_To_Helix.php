#!/usr/local/bin/php
<?php

if (!defined ('EVENT_TYPE')) {
	define ('EVENT_TYPE', '-event-type');
}

if (!defined ('DEVICE_IDENTIFIER')) {
	define ('DEVICE_IDENTIFIER', '-device-identifier');
}

if (!defined ('ANTENNA_IDENTIFIER')) {
	define ('ANTENNA_IDENTIFIER', '-antenna-identifier');
}

if (!defined ('TAG_IDENTIFIER')) {
	define ('TAG_IDENTIFIER', '-tag-identifier');
}

if (!defined ('READ_DATE_TIME')) {
	define ('READ_DATE_TIME', '-read-date-time');
}

function cli_exit ($return, $msg = null)
{
	if (!is_null ($msg)) {
		print ($msg . "\n");
	}
	exit ($return);
}

class dbCLI
{
	private $eventType = null;
	
	private $deviceID = null;
	
	private $antennaID = null;
	
	private $tagID = null;
	
	private $readDateTime = null;
	
	public function __construct ()
	{
		return $this;
	}
	
	public function processParms(Array $args)
	{
		try
		{
			for ($indx = 0; $indx < sizeof ($args); $indx++)
			{
				if ($args[$indx] == strtolower (EVENT_TYPE))
				{
					if ($this->eventType == null)
					{
						$indx++;
						$this->eventType = trim ($args[$indx]);
						print (EVENT_TYPE . ' = ' . $this->eventType . "\n");
					}
					else
					{
						cli_exit (-10, EVENT_TYPE . ' was specified more than once');
					}
				}
				else if ($args[$indx] == strtolower (DEVICE_IDENTIFIER))
				{
					if ($this->deviceID == null)
					{
						$indx++;
						$this->deviceID = trim ($args[$indx]);
						print (DEVICE_IDENTIFIER . ' = ' . $this->deviceID . "\n");
					}
					else
					{
						cli_exit (-10, DEVICE_IDENTIFIER . ' was specified more than once');
					}
				}
				else if ($args[$indx] == strtolower (ANTENNA_IDENTIFIER))
				{
					if ($this->antennaID == null)
					{
						$indx++;
						$this->antennaID = trim ($args[$indx]);
						print (ANTENNA_IDENTIFIER . ' = ' . $this->antennaID . "\n");
					}
					else
					{
						cli_exit (-10, ANTENNA_IDENTIFIER . ' was specified more than once');
					}
				}
				else if ($args[$indx] == strtolower (TAG_IDENTIFIER))
				{
					if ($this->tagID == null)
					{
						$indx++;
						$this->tagID = trim ($args[$indx]);
						print (TAG_IDENTIFIER . ' = ' . $this->tagID . "\n");
					}
					else
					{
						cli_exit (-10, TAG_IDENTIFIER . ' was specified more than once');
					}
				}
				else if ($args[$indx] == strtolower (READ_DATE_TIME))
				{
					if ($this->readDateTime == null)
					{
						$indx++;
						$this->readDateTime = trim ($args[$indx]);
						print (READ_DATE_TIME . ' = ' . $this->readDateTime . "\n");
					}
					else
					{
						cli_exit (-10, READ_DATE_TIME . ' was specified more than once');
					}
				}
			}
		}
		catch (Exception $ex)
		{
			cli_exit (-20, 'A Parameter value is missing.');
		}
		
		//Check that all parameters are specified properly
		if ($this->eventType == null)
		{
			cli_exit (-30, 'Missing ' . EVENT_TYPE . ' param');
		}
		
		if ($this->deviceID == null)
		{
			cli_exit (-30, 'Missing ' . DEVICE_IDENTIFIER . ' param');
		}
		
		if ($this->antennaID == null)
		{
			cli_exit (-30, 'Missing ' . ANTENNA_IDENTIFIER . ' param');
		}
		
		if ($this->tagID == null)
		{
			cli_exit (-30, 'Missing ' . TAG_IDENTIFIER . ' param');
		}
		
		if ($this->readDateTime == null)
		{
			cli_exit (-30, 'Missing ' . READ_DATE_TIME . ' param');
		}
	}
}

/*
 * 
 *
 * named parameters
 *
 * -event-type			=		Read event type
 * -device-identifier	=		Device Identifier
 * -antenna-identifier	=		Antenna Identifier
 * -tag-identifier		=		Tag Identifier
 * -read-date-time		=		Read Date and Time
 *
 */

$parsedCLI = new dbCLI;

$parsedCLI->processParms ($argv);

?>