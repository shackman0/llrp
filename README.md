this software belongs to Floyd Shackelford. 

you are allowed to scrutinize this software for the purposes of evaluating my skills as a software engineer. 
any other use requires advance permission.

Low Level Reader Protocol (LLRP)

this was a fun project i did to demonstrate interfacing with an FX7400 LLRP device.

you won't be able to actually do much with this because you don't have a FX7400 LLRP reader, but you can 
certainly review the source code and run the server (and watch it do nothing).
